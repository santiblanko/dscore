########################## FUNCI�N PRONOSTICAR D�AS EXTREMADAMENTE AT�PICOS ##############################
PronosticarDEA <- function(y_adelante,y_media,y_ref)
{
  # y_adelante es el pron�stico adelante
  # y_media es la serie media de los d�as extremadamente at�picos
  # y_ref es la �ltima serie observada para un d�a extremadamente at�pico
  
  #IAH <- mean((y_ref/y_media-1)^2)
  IAH <- mean(abs(y_ref/y_media-1))
  
  Beta <-  (max(y_adelante) - (1 + IAH) * max(y_media))/(max(y_adelante) - max(y_media))
  
  y_DEA <- (1-Beta) * y_adelante + Beta * y_media

  return(y_DEA)
}
#################################################################################################################