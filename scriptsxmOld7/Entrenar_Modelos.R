source(paste(ruta,'fun_GenerarDatos.R',sep = ''))

indicadores <- c(1:rezago_minutos,(1:rezago_dias)*288)### SIRVE PARA LA FUNCION generar_datos_ciclo

### DEFINIMOS LAS VARIABLES REGRESORAS
nombre_variables_regresoras <- c("HORA","Humedad","Radiacion","Temperatura","Viento",Dias) ### VECTOR CON EL NOMBRE DE LAS VARIABLES REGRESORAS

contMod <- 0
ModNombSeries2 <- NULL
ListModelos <- NULL # Contendra una lista con todos los modelos estimados
for (jj in 1:length(SerPron))
{
  nombMeteor <- c(paste(SerPron[jj],'_h',sep = ''),paste(SerPron[jj],'_r',sep = ''),paste(SerPron[jj],'_t',sep = ''),paste(SerPron[jj],'_v',sep = ''))
  data <- datosBDSIN[,c("FECHA","HORA",SerPron_f[jj],nombMeteor,Dias)]   ### SE EXTRAEN DE LA BASE DE DATOS LAS VARIABLES DE INTERES
  nombData <- colnames(data)
  nombData[4:7] <- c("Humedad","Radiacion","Temperatura","Viento")
  colnames(data) <- nombData
  y <- data[,SerPron_f[jj]]
  
  # Se escalan las variables regresoras y la demanda para el calculo de rezagos (no la variable dependiente)
  data[,SerPron_f[jj]] <- escalar(data[,SerPron_f[jj]],LimitesRegres['Minimo','Demanda',jj],LimitesRegres['Maximo','Demanda',jj],TRUE)
  for (j in 2:ncol(LimitesRegres[,,jj]))
  {
    data[,colnames(LimitesRegres[,,jj])[j]] <- escalar(data[,colnames(LimitesRegres[,,jj])[j]],LimitesRegres['Minimo',j,jj],LimitesRegres['Maximo',j,jj],TRUE)
  }
  
  #### MODELOS DE CORTE TRANSVERSAL ----
  
  if (Entrena_GAM || Entrena_RL)
  {
    ### 1) SACAR LOS DATOS DE ENTRENAMIENTO Y LOS DATOS PARA PRUEBA PARA LOS MODELOS DE CORTE TRANSVERSAL----
    base <- generar_datos(data[,SerPron_f[jj]],rezago_minutos,rezago_dias,rezago_minuto_dias,data = data) ### GENERAMOS LA BASE DE DATOS
    base_datos <- base$data ### SACAMOS LOS DATOS DE LA BASE

    #datos3 <- data.frame(DEMANDA=base_datos[,SerPron_f[jj]],base_datos[,-c(1,3:4)])## LE AGREGAMOS COMO PRIMERA COLUMNA TEBAID, Y ELIMINAMOS DE LA BASE DE DATOS LA FECHA LA HORA Y TEBAID
    datos3 <- data.frame(DEMANDA=base_datos[,SerPron_f[jj]],base_datos[,-c(1,3)])## LE AGREGAMOS COMO PRIMERA COLUMNA TEBAID, Y ELIMINAMOS DE LA BASE DE DATOS LA FECHA LA HORA Y TEBAID
    datos3 <- as.matrix(datos3) ### CONVERTIMOS NUESTRA BASE DE DATOS EN MATRIX
    x1 <- datos3[,-1]
    n=nrow(datos3) ## SACAMOS EL NUMERO DE DATOS QUE TENEMOS
    
    train=seq(1,n-(diasPronostico*288),by=1) ## VECTOR QUE CONTIENE LAS OBSERVACIONES DENTRO DEL CONJUNTO DE ENTRENAMIENTO
    
    ### DEFINIMOS EL CONJUNTO DE ENTRENAMIENTO
    xentrena <- x1[train,]
    yentrena <- tail(datosBDSIN[,SerPron_f[jj]],n)[1:length(train)] # En caso que no se desee escalar
    #yentrena=datos3[train,1] # En caso que si se desee escalar la demanda
    entrenamiento=data.frame(yentrena,xentrena)
    
    ### DEFINIMOS EL CONJUNTO DE PRUEBA
    xtest=x1[-train,]
    ytest<- tail(datosBDSIN[,SerPron_f[jj]],diasPronostico*288)
    #ytest=datos3[-train,1]
    test <- data.frame(ytest,xtest)
  }
  
  ### 2) REGRESION LINEAL----
  if (Entrena_RL & Entrenar)
  {
    print('Inicio estimacion modelo Regresion Lineal')
    modelo_RL <- lm(yentrena~.+I(HORA^2)+I(HORA^3)+I(HORA^4)+I(HORA^5)-1, data = entrenamiento)### AGREGAMOS POLINOMIOS DE LA HORA
    contMod <- contMod + 1
    ListModelos[[contMod]] <- modelo_RL
    ModNombSeries2 <- c(ModNombSeries2,paste(SerPron[jj],'_RL',sep = ''))
    summary(modelo_RL)
    
    f0 <- predict(modelo_RL,data.frame(xtest))
    MAPE2 <- mean(abs(ytest-f0)/abs(ytest))
    #matplot(cbind(ytest, f0),col=c('red','blue'),type='l')
  }
  
  
  ### 3) GAM----  
  if (Entrena_GAM & Entrenar)
  {
    print('Inicio estimacion modelo GAM')
    k=Nudos
    star_date <- Sys.time()
    modelo_GM=gam(as.formula(paste("yentrena~",paste(c("s(HORA ,k=k+4)+s( Humedad ,k=k)+s( Radiacion ,k=k)+s( Temperatura ,k=k)+s( Viento ,k=k)+LUNES+MARTES+MIERCOLES+JUEVES+VIERNES+SABADO+DOMINGO+LF+DFNL",
                                                        paste("s(",c(base$names$rezago,base$names$dias,base$names$rezago_dias,base$names$indicador_demanda),",k=k)"),base$names$indicador),collapse = "+")))
                   ,family=gaussian,data=entrenamiento)
    (cuanto_demoro <- Sys.time()-star_date)
    contMod <- contMod + 1
    ListModelos[[contMod]] <- modelo_GM
    ModNombSeries2 <- c(ModNombSeries2,paste(SerPron[jj],'_GM',sep = ''))
    summary(modelo_GM)
    
    f0 <- predict(modelo_GM,data.frame(xtest))
    MAPE2 <- mean(abs(ytest-f0)/abs(ytest))
    matplot(cbind(ytest, f0),col=c('red','blue'),type='l')
  }
  
  #### MODELOS DE SERIES DE TIEMPO----
  
  if (Entrena_ARIMA || Entrena_ELMAN || Entrena_LSTM)
  {
    ### 1) SACAR LOS DATOS DE ENTRENAMIENTO Y LOS DATOS PARA PRUEBA PARA MODELOS DE SERIES DE TIEMPO ----
    
    base <- generar_datos_series_tiempo(data[,SerPron_f[jj]],rezago_dias,rezago_minuto_dias,data = data)
    base_datos <- base$data### EN ESTE CASO SI SE DESEA SE PUEDE USAR EL CICLO FOR
    
    #datos3 <- data.frame(DEMANDA=base_datos[,4],base_datos[,-c(1,3:4)])## LE AGREGAMOS COMO PRIMERA COLUMNA TEBAID, Y ELIMINAMOS DE LA BASE DE DATOS LA FECHA LA HORA Y TEBAID
    datos3 <- data.frame(DEMANDA=base_datos[,SerPron_f[jj]],base_datos[,-c(1,3)])## LE AGREGAMOS COMO PRIMERA COLUMNA TEBAID, Y ELIMINAMOS DE LA BASE DE DATOS LA FECHA LA HORA Y TEBAID
    datos3 <- as.matrix(datos3) ### CONVERTIMOS NUESTRA BASE DE DATOS EN MATRIX
    x1 <- datos3[,-1]
    
    #x1[,nombre_variables_regresoras] <- apply(x1[,nombre_variables_regresoras], 2, ff)
    n=nrow(datos3) ## SACAMOS EL NUMERO DE DATOS QUE TENEMOS
    
    train=seq(1,n-(diasPronostico*288),by=1) ## VECTOR QUE CONTIENE LAS OBSERVACIONES DENTRO DEL CONJUNTO DE ENTRENAMIENTO
    
    ### DEFINIMOS EL CONJUNTO DE ENTRENAMIENTO
    xentrena=x1[train,]
    yentrena=datos3[train,1]
    entrenamiento=data.frame(yentrena,xentrena)
    
    ### DEFINIMOS EL CONJUNTO DE PRUEBA
    xtest=x1[-train,]
    ytest=datos3[-train,1]
    test <- data.frame(ytest,xtest)
    
    
    ### 2) MODELO ARIMA ----
    if (Entrena_ARIMA & Entrenar)
    {
      print('Inicio estimacion modelo ARIMA')
      nn <- which(colnames(xentrena)=="DOMINGO")
      start_time <- Sys.time()
      modelo_ARIMA <- Arima(yentrena,order = c(Ar,I,Ma),xreg = xentrena[,-nn])
      (duracion <- Sys.time()-start_time)
      contMod <- contMod + 1
      ListModelos[[contMod]] <- modelo_ARIMA
      ModNombSeries2 <- c(ModNombSeries2,paste(SerPron[jj],'_AX',sep = ''))
      
      summary(modelo_ARIMA)
      
      f0 <- as.numeric(forecast(modelo_ARIMA,xreg=xtest[,-nn])$mean)
      MAPE2 <- mean(abs(ytest-f0)/abs(ytest))
      matplot(cbind(ytest, f0),col=c('red','blue'),type='l')
    }
    
    
    ### 3) RED ELMAN----
    if (Entrena_ELMAN & Entrenar)
    {
      print('Inicio estimacion modelo ELMAN')
      modelo_ELMAN <- elman(xentrena,
                            yentrena,
                            size = c(neuronasCapa1Elman,neuronasCapa2Elman),
                            maxit =maxitElman,
                            learnFuncParams = c(learnRateElman),
                            inputsTest=xtest,
                            targetsTest=ytest)
      contMod <- contMod + 1
      ListModelos[[contMod]] <- modelo_ELMAN
      ModNombSeries2 <- c(ModNombSeries2,paste(SerPron[jj],'_EL',sep = ''))
      plotIterativeError(modelo_ELMAN)
      f0 <- predict(modelo_ELMAN,data.frame(xtest))
      MAPE2 <- mean(abs(ytest-f0)/abs(ytest))
      matplot(cbind(ytest, f0),col=c('red','blue'),type='l')
    }
    
    
    ### 4) RED LSTM ----
    if (Entrena_LSTM & Entrenar)
    {
      print('Inicio estimacion modelo LSTM')
      
      x <- xtest[,c(nombre_variables_regresoras)]
      x2 <- xtest[,base$names$indicador]
      dim(xentrena) <- c(nrow(xentrena),1,ncol(xentrena))### REDIMENSIONAMOS LOS DATOS DE ENTRADA
      dim(xtest) <- c(nrow(xtest),1,ncol(xtest))### REDIMENSIONAMOS LOS DATOS DE ENTRADA
      yentrena=as.matrix(yentrena)
      ytest=as.matrix(ytest)
      
      modelo_LSTM <- keras_model_sequential() 
      modelo_LSTM %>% 
        layer_simple_rnn(units = neuronasCapa1LSTM,activation = activacionCapa1LSTM, return_sequences = TRUE,input_shape = c(dim(xentrena)[-1])) %>% ## layer_simple_rnn
        layer_simple_rnn(units = neuronasCapa2LSTM,activation = activacionCapa2LSTM, return_sequences = TRUE) %>% 
        #layer_lstm(units = 20,activation = "relu", return_sequences = TRUE )%>% 
        layer_simple_rnn(units = 10) %>% # return a single vector dimension 50
        layer_dense(units = 1) %>%  
        compile(
          loss = 'mse',
          optimizer = optimizer_rmsprop(lr = learnRateLSTM),
          metrics = c('mse','mape')
        )
      modelo_LSTM %>% fit(
        xentrena, yentrena, batch_size = batchSizeLSTM, epochs = maxitLSTM,validation_data = list(xtest,ytest)
      )
      contMod <- contMod + 1
      ListModelos[[contMod]] <- modelo_LSTM
      ModNombSeries2 <- c(ModNombSeries2,paste(SerPron[jj],'_LS',sep = ''))
      f0 <- predict(modelo_LSTM,xtest)
      MAPE2 <- mean(abs(ytest-f0)/abs(ytest))
      matplot(cbind(ytest, f0),col=c('red','blue'),type='l')
    }
  }
}