

Entrena_GAM <- FALSE
Entrena_RL <- TRUE
Entrena_ARIMA <- FALSE
Entrena_ELMAN <- FALSE
Entrena_LSTM <- FALSE

# Variable de entrada booleana que determina si se realiza escalamiento de la demanda
escalarDemanda <- TRUE # SI QUIERO ESCALAR LOS REZAGOS DE LA DEMANDA

# Variable de entrada booleana que determina si se realiza escalamiento de variables ex�genas
escalarExogenas <- TRUE # SI QUIERO ESCALAR LAS VARIABLES CLIMATOLOGICAS

rezago_minutos <- 15 ### DEFINIMOS CUANTOS REZAGOS DE 5 MINUTOS DESEAMOS
rezago_dias <- 14 ### DEFINIMOS CUANTOS DIAS DE REZAGO DESEAMOS
rezago_minuto_dias <- 3 ### DEFINIMOS CUANTOS REZAGOS DE LA SEMANA ANTERIOR VAMOS A TENER EN CUENT EJEMPLO LAG.2017, LAG.2018

# Variables de entrada binarias 0 - 1 que determinan con qu??? modelos se pronostica
pronostico_RL <- TRUE
pronostico_GAM <- FALSE
pronostico_ARIMA <- FALSE
pronostico_ELMAN <- FALSE
pronostico_LSTM <- FALSE

# Parametros Modelo ARIMA
Ar <- 4
I <- 1
Ma <- 4

# Parametros Modelo ELMAN
capasElman <- 2
neuronasCapa1Elman <- 10
neuronasCapa2Elman <- 10
maxitElman <- 100
learnRateElman <- 0.002

# Parametros Modelo LSTM
capasLSTM <- 2
neuronasCapa1LSTM <- 10
neuronasCapa2LSTM <- 10
activacionCapa1LSTM <- 'relu'
activacionCapa2LSTM <- 'relu'
batchSizeLSTM <- 500
maxitLSTM <- 40
learnRateLSTM <- 0.005

# Nivel de confianza para la imputaci???n de datos en tiempo real. Min = 0.6, Max = 0.99
nivelConf <- 0.95

# Periodicidad de filtrado en tiempo real. Min = 1, Max = 288
nDatosFilt <- 36

# Periodicidad de pronostico en tiempo real. Min = 1, Max = 288
nDatosPron <- 1

nDatosEnt <- 288

# Constante de penalidad del filtro Hodrick-Prescott. Min = 1, Max = 100000
PenFiltrHP <- 50

# Nombres de los modelos estimados
ModNomb <- c('RL','GAM','RN')

# N???mero de d???as adelante a pronosticar. Min = 1, Max = 14
diasPronostico <- 1


# Variable de entrada selector:
metodo_comb <- 3
# 1) Promedio de pron???sticos
# 2) M???nima suma de residuos al cuadrado
# 3) M???nima varianza

# Variable de entrada entera. N???mero de periodos a usar para el c???lculo de errores anteriores al actual en la correcci???nde de pron???sticos. Min = 1, Max = 288
nPerAntCorrec <- 12


# Variable de entrada. Coeficiente de ponderaci???n de errores en la correcci???n de pron???sticos. Min = 0.01, Max = 1.
alphaCorrec <- 0.8


# Variable de entrada entera. N�mero de clusters por hora por tipo de dia. Min = 2 Max = 15.
kClusters <- 10


# Periodicidad de ajuste de clusters en tiempo real. Min = 1, Max = 4032
nDatosClust <- 72

Norma <- 2 # 1, distancia Manhattan, 2 distancia Euclideana

filtrarBarras <- FALSE