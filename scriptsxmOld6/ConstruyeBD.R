# Version del 30 de octubre de 2018

#datos <- read_excel(paste(ruta,"datosDemandas.xlsx", sep= ''))
datos <- read_excel(paste(ruta,"datosDemandasSol.xlsx", sep= ''))
datos_dias <- read.csv(paste(ruta,"Dias.csv", sep= ''))

# CAMILO
############# CAMILO POR FAVOR PEGAR LOS DATOS METEOROLOGICOS REALES A LAS DOS AREAS QUE TENEMOS ##################
datosMeteor <- read.csv(paste(ruta,"Tebaid2.csv", sep= ''))
datosMeteor1 <- cbind(datosMeteor$Humedad,datosMeteor$Temperatura,datosMeteor$Radiacio,datosMeteor$Velocidad_viento)
colnames(datosMeteor1) <- c('Quindio_h','Quindio_t','Quindio_r','Quindio_v')
datosMeteor2 <- cbind(datosMeteor$Humedad,datosMeteor$Temperatura,datosMeteor$Radiacio,datosMeteor$Velocidad_viento)
colnames(datosMeteor2) <- c('Caribe_h','Caribe_t','Caribe_r','Caribe_v')
datosMeteor3 <- cbind(datosMeteor$Humedad,datosMeteor$Temperatura,datosMeteor$Radiacio,datosMeteor$Velocidad_viento)
colnames(datosMeteor3) <- c('SIN_h','SIN_t','SIN_r','SIN_v')

datosMeteor4 <- cbind(datosMeteor$Humedad,datosMeteor$Temperatura,datosMeteor$Radiacio,datosMeteor$Velocidad_viento)
colnames(datosMeteor4) <- c('REGIVI_h','REGIVI_t','REGIVI_r','REGIVI_v')
datosMeteor5 <- cbind(datosMeteor$Humedad,datosMeteor$Temperatura,datosMeteor$Radiacio,datosMeteor$Velocidad_viento)
colnames(datosMeteor5) <- c('ARMENIA_h','ARMENIA_t','ARMENIA_r','ARMENIA_v')
datosMeteor6 <- cbind(datosMeteor$Humedad,datosMeteor$Temperatura,datosMeteor$Radiacio,datosMeteor$Velocidad_viento)
colnames(datosMeteor6) <- c('TEBAID_h','TEBAID_t','TEBAID_r','TEBAID_v')

datosMeteor7 <- cbind(datosMeteor$Humedad,datosMeteor$Temperatura,datosMeteor$Radiacio,datosMeteor$Velocidad_viento)
colnames(datosMeteor7) <- c('URIBIA_h','URIBIA_t','URIBIA_r','URIBIA_v')
datosMeteor8 <- cbind(datosMeteor$Humedad,datosMeteor$Temperatura,datosMeteor$Radiacio,datosMeteor$Velocidad_viento)
colnames(datosMeteor8) <- c('CARTAGENA_h','CARTAGENA_t','CARTAGENA_r','CARTAGENA_v')

datosBDSIN <- cbind(datos,datos_dias,datosMeteor1,datosMeteor2,datosMeteor3,datosMeteor4,datosMeteor5,datosMeteor6,datosMeteor7,datosMeteor8)

datosSIN <- datosBDSIN[1:122112,]

# Se guardan los datos que se iran llamando en tiempo real
#write.csv(datosBDSIN[122113:139680,],paste(ruta,'BD_Demanda_prueba.csv',sep = ''),row.names = F)

remove(datos,datos_dias,datosMeteor,datosMeteor1,datosMeteor2,datosMeteor3,datosMeteor4,datosMeteor5,datosMeteor6,datosMeteor7,datosMeteor8,datosBDSIN)

######################################## AQUI EMPIEZA C�DIGO DE IMPUTACI�N ##############################

alpha <- 1-nivelConf
DiasDatos <- datosSIN[Dias] # Se extraen las variables tipo de dia

y_imp <- NULL
dymin <- NULL
dymax <- NULL
y_medias <- NULL # Contiene la serie media de todas las subestaciones
y_min <- NULL # Contiene la serie de intervalo inferior de todas las subestaciones
y_max <- NULL # Contiene la serie de intervalo inferior de todas las subestaciones


# Se construye el listado de series a imputar
nombSeriesImp <- NULL
if (imputar_Subest)
{
  nombSeriesImp <- c(nombSeriesImp,Subest)
}
if(imputar_Areas)
{
  nombSeriesImp <- c(nombSeriesImp,Areas)
}
if (imputar_SIN)
{
  nombSeriesImp <- c(nombSeriesImp,'SIN')
}

# CREAR NOMBRES PARA DATOS IMPUTADOS
Series_i <- NULL
for (j in 1:length(nombSeriesImp))
{
  Series_i[j] <- paste(nombSeriesImp[j],'_i',sep = '')
}

# CREAR NOMBRES PARA DATOS FILTRADOS
Series_f <- NULL
for (j in 1:length(nombSeriesImp))
{
  Series_f[j] <- paste(nombSeriesImp[j],'_f',sep = '')
}


for (j in 1:length(nombSeriesImp))
{
  # CALCULO DE LA MEDIA DE CADA TIPO DE DIA DE LOS DATOS QUE HAYAN EN LA BASE DE DATOS
  col_subest <- which(colnames(datosSIN)==nombSeriesImp[j]) # Indica la columna de la subestaci�n correspondiente
  y_med <- matrix(NA,nrow = 288,ncol=9) # Matriz de 288 x 9 que contiene el valor promedio de cada tipo de dia en cada periodo
  LimInfDia <- matrix(NA,nrow = 288,ncol=9) # Matriz de 288 x 9 que contiene el l�mite inferior de cada tipo de dia en cada periodo
  LimSupDia <- matrix(NA,nrow = 288,ncol=9) # Matriz de 288 x 9 que contiene el l�mite superior de cada tipo de dia en cada periodo
  for (i in 1:length(Dias))
  {
    data_temp <- subset(datosSIN,datosSIN[Dias[i]]==1) # datos del Dia i
    y_med[,i] <- MediaDia(data_temp[,col_subest])
    LimInfDia[,i] <- CuantilDia(data_temp[,col_subest],alpha/2)
    LimSupDia[,i] <- CuantilDia(data_temp[,col_subest],1-alpha/2)
  }
  y_media <- Vectorizar(y_med,DiasDatos) # vector de la media
  LimInf <- Vectorizar(LimInfDia,DiasDatos) # vector del l�mite inferior
  LimSup <- Vectorizar(LimSupDia,DiasDatos) # vector del l�mite superior
  
  ######################## GUARDAR LA MEDIA Y LOS INTERVALOS DE CONFIANZA PARA ALMACENAR EN BASE DE DATOS #######################
  y_medias <- cbind(y_medias,y_media)
  y_min <- cbind(y_min,LimInf)
  y_max <- cbind(y_max,LimSup)

  ##########################################################################################
  ############################### LLAMADO A LA FUNCI�N IMPUTAR #############################
  ##########################################################################################
  y <- datosSIN[,col_subest]
  y_imp <- cbind(y_imp,Imputar(y,y_media,LimInf,LimSup,2)) # Se imputa usando un criterio particular
  dymin <- cbind(dymin,quantile(Derivar(y_imp[,j]),probs = 0.1)) # Se calcula la m�nima derivada
  dymax <- cbind(dymax,quantile(Derivar(y_imp[,j]),probs = 0.9)) # Se calcula la m�xima derivada
}
LimDerivadas <- rbind(cbind('minim',dymin),cbind('maxim',dymax))
colnames(y_imp) <- Series_i
colnames(LimDerivadas) <- c('',Series_i)


###########################################################################################################
################################ FILTRADO POR TRAMOS DE LOS DATOS IMPUTADOS ###############################
###########################################################################################################
y_filt <- array(numeric(),dim = c(nrow(y_imp),length(Series_f)))
nbloquesFilt <- ceiling(nrow(y_imp)/(nDiasBloque*288))

for (j in 1:length(Series_i))
#for (j in 1:3)
{
  for (b in 1:nbloquesFilt)
  #for (b in 1:5)
  {
    print(paste('Filtrando serie ',Series_i[j], ' bloque ',b,' de ',nbloquesFilt,' ...', sep =''))
    idxDatos <- (nDiasBloque*288*(b-1)+1):min(nrow(y_imp),(nDiasBloque*288*b))
    y_temp <- y_imp[idxDatos,Series_i[j]]
    if (b == 1)
    {
      y_filt2 <- FiltrarHPMod2(y_temp,lambdaBD)
    } else
    {
      y_filt2 <- c(y_filt2,FiltrarHPEmpalme(y_temp,y_ant,lambdaBD))
    }
    y_ant <- tail(y_filt2,2)
  }
  y_filt[,j] <- y_filt2
}

colnames(y_filt) <- Series_f

################################## PEGAR DATOS IMPUTADOS Y FILTRADOS A datosSIN ##########################
datosSIN <- cbind(datosSIN,y_imp,y_filt)
write.csv(datosSIN,paste(ruta,'BD_DemandaSol.csv',sep = ''),row.names = F)
remove(datosSIN)
###########################################################################################################