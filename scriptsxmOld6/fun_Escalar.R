escalar = function(x,minx, maxx, escalarVar)
{
  if (escalarVar)
  {
    y <- (x-minx)/(maxx-minx)
  }
  else
  {
    y <- x
  }
  return(y)
}