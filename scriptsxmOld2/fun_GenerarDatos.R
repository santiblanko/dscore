### ESTA FUNCION SE USA PARA CONSTRUIR DATOS DE ENTRADA EN LA ETAPA DE ESTIMACIÓN DEL MODELO
# y es el vector de demanda
# data es la BD

generar_datos <-  function(y,rezago=5,rezago_dias=7,data=data){
  r <- NULL
  for (i in 1:rezago) {
    c_1 <- Lag(y,i)
    r <- cbind(r,c_1)
  }
  s <- NULL
  for (i in 1:rezago_dias) {
    c_1 <- Lag(y,i*288)
    s <- cbind(s,c_1)
  } 
  s <- s[,-c(7*c(1:floor(rezago_dias/7)))]
  e <- NULL
  e1 <- NULL
  lf <- NULL
  dfnl <- NULL
  pe <- NULL
  dlf <- data$LF
  ddfnl <- data$DFNL
  if(floor(rezago_dias/7)!=0){
    ff <- 2016*(1:floor(rezago_dias/7))
    for (i in 1:length(ff)) {
      for (l in 0:3) {
        c_1 <- Lag(y,ff[i]+l)
        e <- cbind(e,c_1)
      }
    }
    for (i in 1:length(ff)) {
      for (l in 0:3) {
        c_1 <- Lag(y,ff[i]+l)
        e1 <- cbind(e1,c_1)
        c_2 <- Lag(dlf,ff[i]+l)
        lf <- cbind(lf,c_2)
        c_3 <- Lag(ddfnl,ff[i]+l)
        dfnl <- cbind(dfnl,c_3)
      }
    }
    colnames(lf) <- paste("LF",colnames(lf),sep=".")
    colnames(dfnl) <- paste("DFNL",colnames(dfnl),sep=".")
    de <- cbind(e1*lf)
    colnames(de) <- paste("DEMANDA_LF",colnames(de),sep=".")
    dr <- cbind(e1*dfnl)
    colnames(dr) <- paste("DEMANDA_DFNL",colnames(dr),sep=".")
  }
  
  if(floor(rezago_dias/7)!=0){dd <- cbind(data,r,s,e,de,dr,lf,dfnl)}
  if(floor(rezago_dias/7)==0){dd <- cbind(data,r,s)}
  dd <- na.omit(dd)
  names <- list(datos=colnames(data),rezago=colnames(r),dias=colnames(s),rezago_dias=colnames(e),indicador=c(colnames(lf),colnames(dfnl)),indicador_demanda=c(colnames(de),colnames(dr)))
  result <- list(data =dd,names=names)
}


### ESTA FUNCION SE USA PARA CONSTRUIR DATOS DE ENTRADA EN EL CICLO DE PRONÓSTICO
generar_datos_ciclo <-  function(y,rezago=5,rezago_dias=7){
  r <- NULL
  y <- y[(length(y)-(max(indicadores)+100)):length(y)]
  for (i in 1:rezago) {
    c_1 <- Lag(y,i-1)
    r <- cbind(r,c_1)
  }
  s <- NULL
  for (i in 1:rezago_dias) {
    c_1 <- Lag(y,i*288-1)
    s <- cbind(s,c_1)
  } 
  s <- s[,-c(7*c(1:floor(rezago_dias/7)))]
  e <- NULL
  
  if(floor(rezago_dias/7)!=0){
    ff <- 2016*(1:floor(rezago_dias/7))
    for (i in 1:length(ff)) {
      for (l in 0:3) {
        c_1 <- Lag(y,ff[i]+l-1)
        e <- cbind(e,c_1)
      }
    }
  }
  
  dd <- cbind(r,s,e)
  dd <- na.omit(dd)
  dd <- tail.matrix(dd,1)
  dd <- dd[1,]
  ff <- tail.matrix(e,1)
  ff <- ff[1,]
  des <- ff*x2[j,]
  dd=list(dd=dd,des=des)
}