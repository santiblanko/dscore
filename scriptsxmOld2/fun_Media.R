Media <- function(datosArea,SubestNomb,Dias,nivelConf)
{
  alpha <- 1-nivelConf
  y_medias <- NULL
  LimitesInf <- NULL
  LimitesSup <- NULL
  
  # CREAR NOMBRES PARA DATOS FILTRADOS
  SubestNomb_f <- NULL
  for (i in 1:length(SubestNomb))
  {
    SubestNomb_f[i] <-paste(SubestNomb[i],'_f',sep = '')
  }
  
  # CREAR NOMBRES PARA DATOS MEDIOS
  SubestNomb_m <- NULL
  for (i in 1:length(SubestNomb))
  {
    SubestNomb_m[i] <-paste(SubestNomb[i],'_m',sep = '')
  }
  
  # CREAR NOMBRES PARA LIMITES INFERIORES
  SubestNomb_inf <- NULL
  for (i in 1:length(SubestNomb))
  {
    SubestNomb_inf[i] <-paste(SubestNomb[i],'_inf',sep = '')
  }
  
  # CREAR NOMBRES PARA LIMITES SUPERIORES
  SubestNomb_sup <- NULL
  for (i in 1:length(SubestNomb))
  {
    SubestNomb_sup[i] <-paste(SubestNomb[i],'_sup',sep = '')
  }
  
  for (j in 1:length(SubestNomb_f))
  {
    # CALCULO DE LA MEDIA DE CADA TIPO DE DIA DE LOS DATOS QUE HAYAN EN LA BASE DE DATOS
    col_subest <- which(colnames(datosArea)==SubestNomb[j]) # Indica la columna de la subestaci�n correspondiente
    y_med <- matrix(NA,nrow = 288,ncol=9) # Matriz de 288 x 9 que contiene el valor promedio de cada tipo de dia en cada periodo
    LimInfDia <- matrix(NA,nrow = 288,ncol=9) # Matriz de 288 x 9 que contiene el l�mite inferior de cada tipo de dia en cada periodo
    LimSupDia <- matrix(NA,nrow = 288,ncol=9) # Matriz de 288 x 9 que contiene el l�mite superior de cada tipo de dia en cada periodo
    for (i in 1:length(Dias))
    {
      data_temp <- subset(datosArea,datosArea[Dias[i]]==1) # datos del Dia i
      y_med[,i] <- MediaDia(data_temp[,col_subest])
      LimInfDia[,i] <- CuantilDia(data_temp[,col_subest],alpha/2)
      LimSupDia[,i] <- CuantilDia(data_temp[,col_subest],1-alpha/2)
    }
    y_medias <- cbind(y_medias,Vectorizar(y_med,DiasDatos)) # vector de la media
    LimitesInf <- cbind(LimitesInf,Vectorizar(LimInfDia,DiasDatos)) # vector del l�mite inferior
    LimitesSup <- cbind(LimitesSup,Vectorizar(LimSupDia,DiasDatos)) # vector del l�mite superior
  }
  colnames(y_medias) <- SubestNomb_m
  colnames(LimitesInf) <- SubestNomb_inf
  colnames(LimitesSup) <- SubestNomb_sup
  
  Lista <- list("DemandaMedia" = y_medias, "LimiteInferior" = LimitesInf,"LimiteSuperior" = LimitesSup)
  return(Lista)
}

