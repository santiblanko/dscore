########################## FUNCI�N CORRECCI�N ERRORES N PASOS ADELANTE ##############################
Corregir <- function(y_adelante,err,alpha)
{
  # y_adelante es el pron�stico adelante
  # err es el arreglo de errores relativos del pron�stico de los periodos inmediatamente anteriores con respecto a la demanda observada
  # alpha pondera el error acumulado y el �ltimo error. Valores altos se emplean cuando el error es tipo sesgo,
  # valores bajos cuando el error es aleatorio.
  A <- 0
  for (t in 1:length(err))
  {
    A <- (1-alpha)*A + alpha*err[t]
  }
  y_corr <- y_adelante *(1-A)
 return(y_corr)
}
#################################################################################################################
