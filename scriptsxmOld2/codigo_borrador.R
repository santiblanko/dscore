########################################################################################################################################################################################################
########################################### MODELACION #################################################################################################################################################
########################################################################################################################################################################################################
#### 1) SACAR LOS DATOS DE ENTRENAMIENTO Y LOS DATOS PARA PRUEBA----
library(quantmod)#### LIBRERIA PARA SACAR LOS REZAGOS 
#data <-  read.csv("/Users/air/Desktop/BD_Quindio.csv") ### LEO LA BASE DE DATOS QUE GUARDE EN EL NUMERAL 8
###TIENE QUE LLAMARSE DATA PARA QUE FUNCIONE LA FUNCION generar_datos
data <- read.csv("C:/Users/Diego/Documents/R/Demand Forecasting XM/BD_Quindio.csv")

data <- data[,c("FECHA","HORA","MIN","Quindio" ,"Humedad","Radiacion","Temperatura"
                ,"Viento","LUNES","MARTES","MIERCOLES","JUEVES","VIERNES","SABADO","DOMINGO","LF","DFNL")]   
y <- data$Quindio
generar_datos <-  function(y,rezago=5,rezago_dias=7,data=data){
  r <- NULL
  for (i in 1:rezago) {
    c_1 <- Lag(y,i)
    r <- cbind(r,c_1)
  }
  s <- NULL
  for (i in 1:rezago_dias) {
    c_1 <- Lag(y,i*288)
    s <- cbind(s,c_1)
  } 
  s <- s[,-c(7*c(1:floor(rezago_dias/7)))]
  e <- NULL
  e1 <- NULL
  lf <- NULL
  dfnl <- NULL
  pe <- NULL
  dlf <- data$LF
  ddfnl <- data$DFNL
  if(floor(rezago_dias/7)!=0){
    ff <- 2016*(1:floor(rezago_dias/7))
    for (i in 1:length(ff)) {
      for (l in 0:3) {
        c_1 <- Lag(y,ff[i]+l)
        e <- cbind(e,c_1)
      }
    }
    for (i in 1:length(ff)) {
      for (l in 0:3) {
        c_1 <- Lag(y,ff[i]+l)
        e1 <- cbind(e1,c_1)
        c_2 <- Lag(dlf,ff[i]+l)
        lf <- cbind(lf,c_2)
        c_3 <- Lag(ddfnl,ff[i]+l)
        dfnl <- cbind(dfnl,c_3)
      }
    }
    colnames(lf) <- paste("LF",colnames(lf),sep=".")
    colnames(dfnl) <- paste("DFNL",colnames(dfnl),sep=".")
    de <- cbind(e1*lf)
    colnames(de) <- paste("DEMANDA_LF",colnames(de),sep=".")
    dr <- cbind(e1*dfnl)
    colnames(dr) <- paste("DEMANDA_DFNL",colnames(dr),sep=".")
  }
  
  if(floor(rezago_dias/7)!=0){dd <- cbind(data,r,s,e,de,dr,lf,dfnl)}
  if(floor(rezago_dias/7)==0){dd <- cbind(data,r,s)}
  dd <- na.omit(dd)
  names <- list(datos=colnames(data),rezago=colnames(r),dias=colnames(s),rezago_dias=colnames(e),indicador=c(colnames(lf),colnames(dfnl)),indicador_demanda=c(colnames(de),colnames(dr)))
  result <- list(data =dd,names=names)
}

rezago_minutos <- 15 ### DEFINIMOS CUANTOS REZAGOS DE 5 MINUTOS DESEAMOS
rezago_dias <- 14 ### DEFINIMOS CUANTOS DIAS DE REZAGO DESEAMOS

indicadores <- c(1:rezago_minutos,(1:rezago_dias)*288)### SIRVE PARA LA FUNCION generar_datos_ciclo

generar_datos_ciclo <-  function(y,rezago=5,rezago_dias=7){
  r <- NULL
  y <- y[(length(y)-(max(indicadores)+100)):length(y)]
  for (i in 1:rezago) {
    c_1 <- Lag(y,i-1)
    r <- cbind(r,c_1)
  }
  s <- NULL
  for (i in 1:rezago_dias) {
    c_1 <- Lag(y,i*288-1)
    s <- cbind(s,c_1)
  } 
  s <- s[,-c(7*c(1:floor(rezago_dias/7)))]
  e <- NULL
  
  if(floor(rezago_dias/7)!=0){
    ff <- 2016*(1:floor(rezago_dias/7))
    for (i in 1:length(ff)) {
      for (l in 0:3) {
        c_1 <- Lag(y,ff[i]+l-1)
        e <- cbind(e,c_1)
      }
    }
  }
  
  dd <- cbind(r,s,e)
  dd <- na.omit(dd)
  dd <- tail.matrix(dd,1)
  dd <- dd[1,]
  ff <- tail.matrix(e,1)
  ff <- ff[1,]
  des <- ff*x2[j,]
  dd=list(dd=dd,des=des)
}### ESTA FUNCION AYUDA EN EL CICLO FOR 

base <- generar_datos(data$Quindio,rezago_minutos,rezago_dias,data = data)
base_datos <- base$data### EN ESTE CASO SI SE DESEA SE PUEDE USAR EL CICLO FOR

datos3 <- data.frame(Quindio=base_datos[,4],base_datos[,-c(1,3:4)])## LE AGREGAMOS COMO PRIMERA COLUMNA TEBAID, Y ELIMINAMOS DE LA BASE DE DATOS LA FECHA LA HORA Y TEBAID

datos3 <- as.matrix(datos3) ### CONVERTIMOS NUESTRA BASE DE DATOS EN MATRIX


f=function(x){(x-min(x))/(max(x)-min(x))} ### FUNCION PARA NORMALIZAR LOS DATOS
x=apply(datos3[,-1],2,f) ## LE APLICAMOS LA FUNCION A LAS VARIABLES REGRSORAS
min_x <- min(datos3[,1]) ## SACAMOS EL MINIMO DE LA SERIE
max_x <- max(datos3[,1]) ## SACAMOS EL MAXIMO DE LA SERIE
n=nrow(datos3) ## SACAMOS EL NUMERO DE DATOS QUE TENEMOS

dias_pronostico = 9 ### DEFINIMOS CUANTOS DIAS QUEREMOS DEJAR POR FUERA DE MUESTRA

train=seq(1,n-(dias_pronostico*288),by=1) ## VECTOR QUE CONTIENE LAS OBSERVACIONES DENTRO DEL CONJUNTO DE ENTRENAMIENTO

### DEFINIMOS EL CONJUNTO DE ENTRENAMIENTO
xentrena=x[train,]
yentrena=datos3[train,1]
entrenamiento=data.frame(yentrena,xentrena)

### DEFINIMOS EL CONJUNTO DE PRUEBA
xtest=x[-train,]
ytest=datos3[-train,1]
test <- data.frame(ytest,xtest)





#### 2) MODELOS -----
#### 2.1) REGRESION LINEAL----
model <- lm(yentrena~.+I(HORA^2)+I(HORA^3)+I(HORA^4)+I(HORA^5)-1
            #+I(LF.Lag.2016*Lag.2016)+I(LF.Lag.2017*Lag.2017)+I(LF.Lag.2018*Lag.2018)+I(LF.Lag.2019*Lag.2019)+
            #I(LF.Lag.4032*Lag.4032)+I(LF.Lag.4033*Lag.4033)+I(LF.Lag.4034*Lag.4034)+I(LF.Lag.4035*Lag.4035)+
            #I(DFNL.Lag.2016*Lag.2016)+I(DFNL.Lag.2017*Lag.2017)+I(DFNL.Lag.2018*Lag.2018)+I(DFNL.Lag.2019*Lag.2019)+
            #I(DFNL.Lag.4032*Lag.4032)+I(DFNL.Lag.4033*Lag.4033)+I(DFNL.Lag.4034*Lag.4034)+I(DFNL.Lag.4035*Lag.4035)
            , data = entrenamiento)### AGREGAMOS POLINOMIOS DE LA HORA
summary(model)

f <- predict(model,data.frame(xtest))
MAPE2 <- mean(abs(ytest-f)/abs(ytest))
matplot(cbind(ytest, f),col=c('red','blue'),type='l')


y <- yentrena
x <- xtest[,c("HORA","Humedad","Radiacion","Temperatura","Viento","LUNES","MARTES",
              "MIERCOLES","JUEVES","VIERNES","SABADO","DOMINGO","LF","DFNL")]
x1 <- xtest[,base$names$indicador_demanda]
x2 <- xtest[,base$names$indicador]
y_pronosticada <- NULL

for (j in 616:(dias_pronostico*288)) {
  x_1 <- NULL
  x_1 <- generar_datos_ciclo(y,rezago_minutos,rezago_dias)
  x_2 <- x_1$des
  x_1 <- (x_1$dd-min_x)/(max_x-min_x)
  x_reg <- as.data.frame(t(c(x[j,],x_1,x_2,x2[j,])),byrow=T);colnames(x_reg)=colnames(xtest)
  y_p <-predict(model,x_reg)
  y_pronosticada <- c(y_pronosticada,y_p)
  y <- c(y,y_p);print(paste("Procesando el dato",j))
}

plot(ytest,ylab = "Demanda",xlab = "",main = "Esperada vs Expectada",type="l")
lines(y_pronosticada,lty=2,col=2)
legend(x="topleft",legend = c("Pronosticada","Real"),col=c(2,1),lty=c(2,1),bty = "n")

MAPE3 <- mean(abs(ytest-y_pronosticada)/abs(ytest))
MAPE3
#### 2.3) GAM! POR DEFECTO ESTA CON REZAGO_MINUTOS=15 Y REZAGO_DIAS=14 SI SE MODIFICA, TAMBIEN MODIFICAR EL SPLINE !    ----  
library(mgcv)
k=3
star_date <- Sys.time()
model=gam(yentrena~s(HORA,k=k+4)+s(Humedad,k=k)+s(Radiacion,k=k)+s(Temperatura,k=k)+s(Viento,k=k)+LUNES +MARTES +MIERCOLES +JUEVES +VIERNES +SABADO +DOMINGO +
            LF +DFNL +s(Lag.1,k=k)+s(Lag.2,k=k)+s(Lag.3,k=k)+s(Lag.4,k=k)+s(Lag.5,k=k)+s(Lag.6,k=k)+s(Lag.7,k=k)+s(Lag.8,k=k)+s(Lag.9,k=k)
          +s(Lag.10,k=k)+s(Lag.11,k=k)+s(Lag.12,k=k)+s(Lag.13,k=k)+s(Lag.14,k=k)+s(Lag.15,k=k)+s(Lag.288,k=k)+s(Lag.576,k=k)+s(Lag.864,k=k)+s(Lag.1152,k=k)+s(Lag.1440,k=k)
          +s(Lag.1728,k=k)+s(Lag.2016,k=k)+s(Lag.2304,k=k)+s(Lag.2592,k=k)+s(Lag.2880,k=k)+s(Lag.3168,k=k)+s(Lag.3456,k=k)
          +s(Lag.3744,k=k)+s(Lag.4032,k=k)+s(Lag.2017,k=k)+s(Lag.2018,k=k)+s(Lag.2019,k=k)+s(Lag.4033,k=k)+s(Lag.4034,k=k)+s(Lag.4035,k=k)
          +LF.Lag.2016+LF.Lag.2017+LF.Lag.2018+LF.Lag.2019+LF.Lag.4032+LF.Lag.4033+LF.Lag.4034+LF.Lag.4035+DFNL.Lag.2016+DFNL.Lag.2017+DFNL.Lag.2018+DFNL.Lag.2019+DFNL.Lag.4032+DFNL.Lag.4033+DFNL.Lag.4034+DFNL.Lag.4035+
            +s(DEMANDA_LF.Lag.2016,k=k)+s(DEMANDA_LF.Lag.2017 ,k=k)+s( DEMANDA_LF.Lag.2018  ,k=k)+s( DEMANDA_LF.Lag.2019  ,k=k)+s( DEMANDA_LF.Lag.4032 ,k=k)+s( DEMANDA_LF.Lag.4033 ,k=k)+s( DEMANDA_LF.Lag.4034 ,k=k)
          +s(DEMANDA_LF.Lag.4035 ,k=k)+s(DEMANDA_DFNL.Lag.2016 ,k=k)+s( DEMANDA_DFNL.Lag.2017 ,k=k)+s(DEMANDA_DFNL.Lag.2018 ,k=k)+s( DEMANDA_DFNL.Lag.2019 ,k=k)+s( DEMANDA_DFNL.Lag.4032 ,k=k)+s(DEMANDA_DFNL.Lag.4033 ,k=k)
          +s( DEMANDA_DFNL.Lag.4034 ,k=k)+s( DEMANDA_DFNL.Lag.4035,k=k)
          ,family=gaussian,data=entrenamiento)
(cuanto_demoro <- Sys.time()-star_date)
summary(model)

f <- predict(model,data.frame(xtest))
MAPE2 <- mean(abs(ytest-f)/abs(ytest))
matplot(cbind(ytest, f),col=c('red','blue'),type='l')

y <- yentrena
x <- xtest[,c("HORA","Humedad","Radiacion","Temperatura","Viento","LUNES","MARTES",
              "MIERCOLES","JUEVES","VIERNES","SABADO","DOMINGO","LF","DFNL")]
x1 <- xtest[,base$names$indicador_demanda]
x2 <- xtest[,base$names$indicador]
y_pronosticada <- NULL

for (j in 1:(dias_pronostico*288)) {
  x_1 <- NULL
  x_1 <- generar_datos_ciclo(y,rezago_minutos,rezago_dias)
  x_2 <- x_1$des
  x_1 <- (x_1$dd-min_x)/(max_x-min_x)
  x_reg <- as.data.frame(t(c(x[j,],x_1,x_2,x2[j,])),byrow=T);colnames(x_reg)=colnames(xtest)
  y_p <-predict(model,x_reg)
  y_pronosticada <- c(y_pronosticada,y_p)
  y <- c(y,y_p);print(paste("Procesando el dato",j))
}

plot(ytest,ylab = "Demanda",xlab = "",main = "Esperada vs Expectada",type="l")
lines(y_pronosticada,lty=2,col=2)
legend(x="topleft",legend = c("Pronosticada","Real"),col=c(2,1),lty=c(2,1),bty = "n")

MAPE3 <- mean(abs(ytest-y_pronosticada)/abs(ytest))
MAPE3