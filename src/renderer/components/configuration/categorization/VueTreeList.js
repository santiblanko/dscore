! function(A, e) {
    "object" == typeof exports && "object" == typeof module ? module.exports = e() : "function" == typeof define && define.amd ? define([], e) : "object" == typeof exports ? exports.VueTreeList = e() : A.VueTreeList = e()
}(this, function() {
    return function(A) {
        function e(M) {
            if (t[M]) return t[M].exports;
            var i = t[M] = {
                exports: {},
                id: M,
                loaded: !1
            };
            return A[M].call(i.exports, i, i.exports, e), i.loaded = !0, i.exports
        }
        var t = {};
        return e.m = A, e.c = t, e.p = "", e(0)
    }([function(A, e, t) {
        "use strict";
        e.VueTreeList = t(1), e.TreeNode = t(13).TreeNode, e.Tree = t(13).Tree
    }, function(A, e, t) {
        t(2);
        var M = t(11)(t(12), t(15), "data-v-713bd170", null);
        A.exports = M.exports
    }, function(A, e, t) {
        var M = t(3);
        "string" == typeof M && (M = [
            [A.id, M, ""]
        ]), M.locals && (A.exports = M.locals);
        t(9)("0727e1c8", M, !0)
    }, function(A, e, t) {
        e = A.exports = t(4)(), e.push([A.id, "@font-face{font-family:icomoon;src:url(" + t(5) + ");src:url(" + t(5) + '#iefix) format("embedded-opentype"),url(' + t(6) + ') format("truetype"),url(' + t(7) + ') format("woff"),url(' + t(8) + '#icomoon) format("svg");font-weight:400;font-style:normal}.vue-tree-icon[data-v-713bd170]{font-family:icomoon!important;speak:none;font-style:normal;font-weight:400;font-variant:normal;text-transform:none;line-height:1;-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale}.vue-tree-icon.item-icon[data-v-713bd170]{margin-right:4px}.vue-tree-icon.item-icon[data-v-713bd170]:hover{color:inherit}.vue-tree-icon[data-v-713bd170]:hover{color:blue}.icon-file[data-v-713bd170]:before{content:"\\E906"}.icon-folder[data-v-713bd170]:before{content:"\\E907"}.icon-caret-down[data-v-713bd170]:before{content:"\\E900"}.icon-caret-right[data-v-713bd170]:before{content:"\\E901"}.icon-edit[data-v-713bd170]:before{content:"\\E902"}.icon-folder-plus-e[data-v-713bd170]:before{content:"\\E903"}.icon-plus[data-v-713bd170]:before{content:"\\E904"}.icon-trash[data-v-713bd170]:before{content:"\\E905"}.border[data-v-713bd170]{height:5px}.border.up[data-v-713bd170]{margin-top:-5px}.border.bottom[data-v-713bd170],.border.up[data-v-713bd170]{background-color:transparent}.border.active[data-v-713bd170]{border-bottom:3px dashed blue}.tree-node[data-v-713bd170]{display:-webkit-box;display:-webkit-flex;display:flex;-webkit-box-align:center;-webkit-align-items:center;align-items:center;padding:5px 0 5px 1rem}.tree-node .input[data-v-713bd170]{border:none;max-width:150px;border-bottom:1px solid blue}.tree-node[data-v-713bd170]:hover{background-color:#f0f0f0}.tree-node.active[data-v-713bd170]{outline:2px dashed pink}.tree-node .caret[data-v-713bd170]{margin-left:-1rem}.tree-node .operation[data-v-713bd170]{margin-left:2rem;letter-spacing:1px}.item[data-v-713bd170]{cursor:pointer}.tree-margin[data-v-713bd170]{margin-left:2em}', ""])
    }, function(A, e) {
        A.exports = function() {
            var A = [];
            return A.toString = function() {
                for (var A = [], e = 0; e < this.length; e++) {
                    var t = this[e];
                    t[2] ? A.push("@media " + t[2] + "{" + t[1] + "}") : A.push(t[1])
                }
                return A.join("")
            }, A.i = function(e, t) {
                "string" == typeof e && (e = [
                    [null, e, ""]
                ]);
                for (var M = {}, i = 0; i < this.length; i++) {
                    var n = this[i][0];
                    "number" == typeof n && (M[n] = !0)
                }
                for (i = 0; i < e.length; i++) {
                    var o = e[i];
                    "number" == typeof o[0] && M[o[0]] || (t && !o[2] ? o[2] = t : t && (o[2] = "(" + o[2] + ") and (" + t + ")"), A.push(o))
                }
            }, A
        }
    }, function(A, e) {
        A.exports = "data:application/vnd.ms-fontobject;base64,4AgAADwIAAABAAIAAAAAAAAAAAAAAAAAAAABAJABAAAAAExQAAAAAAAAAAAAAAAAAAAAAAEAAAAAAAAA07p/gAAAAAAAAAAAAAAAAAAAAAAAAA4AaQBjAG8AbQBvAG8AbgAAAA4AUgBlAGcAdQBsAGEAcgAAABYAVgBlAHIAcwBpAG8AbgAgADEALgAwAAAADgBpAGMAbwBtAG8AbwBuAAAAAAAAAQAAAAsAgAADADBPUy8yDxIF+AAAALwAAABgY21hcBdW0o4AAAEcAAAAVGdhc3AAAAAQAAABcAAAAAhnbHlmXV8IXAAAAXgAAARUaGVhZA5OKqMAAAXMAAAANmhoZWEHwgPNAAAGBAAAACRobXR4JgAEfAAABigAAAAwbG9jYQWkBEwAAAZYAAAAGm1heHAAEgBwAAAGdAAAACBuYW1lmUoJ+wAABpQAAAGGcG9zdAADAAAAAAgcAAAAIAADA8cBkAAFAAACmQLMAAAAjwKZAswAAAHrADMBCQAAAAAAAAAAAAAAAAAAAAEQAAAAAAAAAAAAAAAAAAAAAEAAAOkHA8D/wABAA8AAQAAAAAEAAAAAAAAAAAAAACAAAAAAAAMAAAADAAAAHAABAAMAAAAcAAMAAQAAABwABAA4AAAACgAIAAIAAgABACDpB//9//8AAAAAACDpAP/9//8AAf/jFwQAAwABAAAAAAAAAAAAAAABAAH//wAPAAEAAAAAAAAAAAACAAA3OQEAAAAAAQAAAAAAAAAAAAIAADc5AQAAAAABAAAAAAAAAAAAAgAANzkBAAAAAAEBMgCbAnsC5QASAAABFAcBBiMiJyY1ETQ3NjMyFwEWAnsL/wAKDw8LCwsLDw8KAQALAcAPC/8ACwsLDwIADwsLC/8ACwAAAAEA2wEuAyUCdwASAAABIicBJjU0NzYzITIXFhUGBwEGAgAPC/8ACwsLDwIADwsLAQr/AAsBLgsBAAoPDwsLCwsPDwr/AAsAAAIAqQCCA0QDHAAmADQAAAEiBhURFAYjISImNRE0NjMhMjY1NCYjISIGFREUFjMhMjY1ETQmIwUWMjcBNjQnJiIHAQYUAzAIDBgR/gcQGBgQAVgIDAwI/pQZJCQZAiIZIwwI/nEGEQYBawYGBhEG/pUGAlIMCP6UEBgYEAH5ERgMCAgMIxn93hkjIxkBgAgMxQYGAW0GEQYGBv6TBhEAAAACAAAAjgQAA8AAJgBGAAABIScuAQcjIgYVERczHgEzITUhBxE0NjM3Fx4BMyEyFh0BMzU0JiMTIzU0JiMiBh0BIyIGFRQWOwEVFBYzMjY9ATMyNjU0JgOy/dwrDRwF5yEtBhQIEwoB/f4SDQgF4ysFGBICJwYHQS0hKo4VDg8Vjg8VFQ+OFQ8OFY4PFRUDdDQQCQEuIP1QKAYGQgECowUHATUIDwcFy8sgLf4Tjg8VFQ+OFQ4PFY4PFRUPjhUPDhUAAAAAAQCAAEADfwM9ACAAAAEhETQmIyIGFREhIgYVBhYzIREUFjMyNjURITI2NTQmIwNf/sMTDQ0T/r4OEgETDQFDEw0NEwE9DRMSDgHfAT4NExMN/sITDQ0T/sEOEhIOAT8SDg0TAAUAQAAAA8ADgAASACQARABPAG0AACUiJjURNDYzMhYVETgBMRQGIzEjIiY1ETQ2MzIWFRE4ATEUBiMBIzU0JiMhIgYdASMiBhUUFjMhOAExMjY1OAE5ATQmIyU0NjMhMhYdASE1ASEiJjURNDYzMhYVERQWMyEyNjURNDYzMhYVERQGAmANExMNDRMTDcANExMNDRMTDQIAoDgn/r8oOKANExMNA0ANExMN/aATDQFBDRL+gAGg/kAoOBMNDRMTDQHADhITDQ4SOMATDQFgDRMTDf6gDRMTDQFgDRMTDf6gDRMCIEAoODgoQBMNDRMTDQ0TQA0TEw1AQPzgOCgB4A4SEg7+IA0TEw0B3w0TEw3+ISg4AAMAvwBAAz8DQAAFAAgADwAAASERIREnHwEjAREhFTMRIQJt/lICgNITUlL+fwFAwP4AA0D9AAIt021T/gECf8D+QQAAAwBHAEcDsgM2ABUAGgAfAAABIScuASsBIgYVERQWMyEyNjURNCYjITUzFyEVIREhEQOI/miNBhIKzxIZGRIDFhEZGRH89MBi/t4DAfz/Aq92CAkZEv1mEhgYEgIUERlSUjX+AgH+AAABAAAAAAAAgH+6018PPPUACwQAAAAAANWYcvMAAAAA1Zhy8wAAAAAEAAPAAAAACAACAAAAAAAAAAEAAAPA/8AAAAQAAAAAAAQAAAEAAAAAAAAAAAAAAAAAAAAMBAAAAAAAAAAAAAAAAgAAAAQAATIEAADbBAAAqQQAAAAEAACABAAAQAQAAL8EAABHAAAAAAAKABQAHgBCAGYAtgEYAUoB1AH2AioAAAABAAAADABuAAUAAAAAAAIAAAAAAAAAAAAAAAAAAAAAAAAADgCuAAEAAAAAAAEABwAAAAEAAAAAAAIABwBgAAEAAAAAAAMABwA2AAEAAAAAAAQABwB1AAEAAAAAAAUACwAVAAEAAAAAAAYABwBLAAEAAAAAAAoAGgCKAAMAAQQJAAEADgAHAAMAAQQJAAIADgBnAAMAAQQJAAMADgA9AAMAAQQJAAQADgB8AAMAAQQJAAUAFgAgAAMAAQQJAAYADgBSAAMAAQQJAAoANACkaWNvbW9vbgBpAGMAbwBtAG8AbwBuVmVyc2lvbiAxLjAAVgBlAHIAcwBpAG8AbgAgADEALgAwaWNvbW9vbgBpAGMAbwBtAG8AbwBuaWNvbW9vbgBpAGMAbwBtAG8AbwBuUmVndWxhcgBSAGUAZwB1AGwAYQByaWNvbW9vbgBpAGMAbwBtAG8AbwBuRm9udCBnZW5lcmF0ZWQgYnkgSWNvTW9vbi4ARgBvAG4AdAAgAGcAZQBuAGUAcgBhAHQAZQBkACAAYgB5ACAASQBjAG8ATQBvAG8AbgAuAAAAAwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=="
    }, function(A, e) {
        A.exports = "data:application/x-font-ttf;base64,AAEAAAALAIAAAwAwT1MvMg8SBfgAAAC8AAAAYGNtYXAXVtKOAAABHAAAAFRnYXNwAAAAEAAAAXAAAAAIZ2x5Zl1fCFwAAAF4AAAEVGhlYWQOTiqjAAAFzAAAADZoaGVhB8IDzQAABgQAAAAkaG10eCYABHwAAAYoAAAAMGxvY2EFpARMAAAGWAAAABptYXhwABIAcAAABnQAAAAgbmFtZZlKCfsAAAaUAAABhnBvc3QAAwAAAAAIHAAAACAAAwPHAZAABQAAApkCzAAAAI8CmQLMAAAB6wAzAQkAAAAAAAAAAAAAAAAAAAABEAAAAAAAAAAAAAAAAAAAAABAAADpBwPA/8AAQAPAAEAAAAABAAAAAAAAAAAAAAAgAAAAAAADAAAAAwAAABwAAQADAAAAHAADAAEAAAAcAAQAOAAAAAoACAACAAIAAQAg6Qf//f//AAAAAAAg6QD//f//AAH/4xcEAAMAAQAAAAAAAAAAAAAAAQAB//8ADwABAAAAAAAAAAAAAgAANzkBAAAAAAEAAAAAAAAAAAACAAA3OQEAAAAAAQAAAAAAAAAAAAIAADc5AQAAAAABATIAmwJ7AuUAEgAAARQHAQYjIicmNRE0NzYzMhcBFgJ7C/8ACg8PCwsLCw8PCgEACwHADwv/AAsLCw8CAA8LCwv/AAsAAAABANsBLgMlAncAEgAAASInASY1NDc2MyEyFxYVBgcBBgIADwv/AAsLCw8CAA8LCwEK/wALAS4LAQAKDw8LCwsLDw8K/wALAAACAKkAggNEAxwAJgA0AAABIgYVERQGIyEiJjURNDYzITI2NTQmIyEiBhURFBYzITI2NRE0JiMFFjI3ATY0JyYiBwEGFAMwCAwYEf4HEBgYEAFYCAwMCP6UGSQkGQIiGSMMCP5xBhEGAWsGBgYRBv6VBgJSDAj+lBAYGBAB+REYDAgIDCMZ/d4ZIyMZAYAIDMUGBgFtBhEGBgb+kwYRAAAAAgAAAI4EAAPAACYARgAAASEnLgEHIyIGFREXMx4BMyE1IQcRNDYzNxceATMhMhYdATM1NCYjEyM1NCYjIgYdASMiBhUUFjsBFRQWMzI2PQEzMjY1NCYDsv3cKw0cBechLQYUCBMKAf3+Eg0IBeMrBRgSAicGB0EtISqOFQ4PFY4PFRUPjhUPDhWODxUVA3Q0EAkBLiD9UCgGBkIBAqMFBwE1CA8HBcvLIC3+E44PFRUPjhUODxWODxUVD44VDw4VAAAAAAEAgABAA38DPQAgAAABIRE0JiMiBhURISIGFQYWMyERFBYzMjY1ESEyNjU0JiMDX/7DEw0NE/6+DhIBEw0BQxMNDRMBPQ0TEg4B3wE+DRMTDf7CEw0NE/7BDhISDgE/Eg4NEwAFAEAAAAPAA4AAEgAkAEQATwBtAAAlIiY1ETQ2MzIWFRE4ATEUBiMxIyImNRE0NjMyFhUROAExFAYjASM1NCYjISIGHQEjIgYVFBYzITgBMTI2NTgBOQE0JiMlNDYzITIWHQEhNQEhIiY1ETQ2MzIWFREUFjMhMjY1ETQ2MzIWFREUBgJgDRMTDQ0TEw3ADRMTDQ0TEw0CAKA4J/6/KDigDRMTDQNADRMTDf2gEw0BQQ0S/oABoP5AKDgTDQ0TEw0BwA4SEw0OEjjAEw0BYA0TEw3+oA0TEw0BYA0TEw3+oA0TAiBAKDg4KEATDQ0TEw0NE0ANExMNQED84DgoAeAOEhIO/iANExMNAd8NExMN/iEoOAADAL8AQAM/A0AABQAIAA8AAAEhESERJx8BIwERIRUzESECbf5SAoDSE1JS/n8BQMD+AANA/QACLdNtU/4BAn/A/kEAAAMARwBHA7IDNgAVABoAHwAAASEnLgErASIGFREUFjMhMjY1ETQmIyE1MxchFSERIREDiP5ojQYSCs8SGRkSAxYRGRkR/PTAYv7eAwH8/wKvdggJGRL9ZhIYGBICFBEZUlI1/gIB/gAAAQAAAAAAAIB/utNfDzz1AAsEAAAAAADVmHLzAAAAANWYcvMAAAAABAADwAAAAAgAAgAAAAAAAAABAAADwP/AAAAEAAAAAAAEAAABAAAAAAAAAAAAAAAAAAAADAQAAAAAAAAAAAAAAAIAAAAEAAEyBAAA2wQAAKkEAAAABAAAgAQAAEAEAAC/BAAARwAAAAAACgAUAB4AQgBmALYBGAFKAdQB9gIqAAAAAQAAAAwAbgAFAAAAAAACAAAAAAAAAAAAAAAAAAAAAAAAAA4ArgABAAAAAAABAAcAAAABAAAAAAACAAcAYAABAAAAAAADAAcANgABAAAAAAAEAAcAdQABAAAAAAAFAAsAFQABAAAAAAAGAAcASwABAAAAAAAKABoAigADAAEECQABAA4ABwADAAEECQACAA4AZwADAAEECQADAA4APQADAAEECQAEAA4AfAADAAEECQAFABYAIAADAAEECQAGAA4AUgADAAEECQAKADQApGljb21vb24AaQBjAG8AbQBvAG8AblZlcnNpb24gMS4wAFYAZQByAHMAaQBvAG4AIAAxAC4AMGljb21vb24AaQBjAG8AbQBvAG8Abmljb21vb24AaQBjAG8AbQBvAG8AblJlZ3VsYXIAUgBlAGcAdQBsAGEAcmljb21vb24AaQBjAG8AbQBvAG8AbkZvbnQgZ2VuZXJhdGVkIGJ5IEljb01vb24uAEYAbwBuAHQAIABnAGUAbgBlAHIAYQB0AGUAZAAgAGIAeQAgAEkAYwBvAE0AbwBvAG4ALgAAAAMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA="
    }, function(A, e) {
        A.exports = "data:application/font-woff;base64,d09GRgABAAAAAAiIAAsAAAAACDwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABPUy8yAAABCAAAAGAAAABgDxIF+GNtYXAAAAFoAAAAVAAAAFQXVtKOZ2FzcAAAAbwAAAAIAAAACAAAABBnbHlmAAABxAAABFQAAARUXV8IXGhlYWQAAAYYAAAANgAAADYOTiqjaGhlYQAABlAAAAAkAAAAJAfCA81obXR4AAAGdAAAADAAAAAwJgAEfGxvY2EAAAakAAAAGgAAABoFpARMbWF4cAAABsAAAAAgAAAAIAASAHBuYW1lAAAG4AAAAYYAAAGGmUoJ+3Bvc3QAAAhoAAAAIAAAACAAAwAAAAMDxwGQAAUAAAKZAswAAACPApkCzAAAAesAMwEJAAAAAAAAAAAAAAAAAAAAARAAAAAAAAAAAAAAAAAAAAAAQAAA6QcDwP/AAEADwABAAAAAAQAAAAAAAAAAAAAAIAAAAAAAAwAAAAMAAAAcAAEAAwAAABwAAwABAAAAHAAEADgAAAAKAAgAAgACAAEAIOkH//3//wAAAAAAIOkA//3//wAB/+MXBAADAAEAAAAAAAAAAAAAAAEAAf//AA8AAQAAAAAAAAAAAAIAADc5AQAAAAABAAAAAAAAAAAAAgAANzkBAAAAAAEAAAAAAAAAAAACAAA3OQEAAAAAAQEyAJsCewLlABIAAAEUBwEGIyInJjURNDc2MzIXARYCewv/AAoPDwsLCwsPDwoBAAsBwA8L/wALCwsPAgAPCwsL/wALAAAAAQDbAS4DJQJ3ABIAAAEiJwEmNTQ3NjMhMhcWFQYHAQYCAA8L/wALCwsPAgAPCwsBCv8ACwEuCwEACg8PCwsLCw8PCv8ACwAAAgCpAIIDRAMcACYANAAAASIGFREUBiMhIiY1ETQ2MyEyNjU0JiMhIgYVERQWMyEyNjURNCYjBRYyNwE2NCcmIgcBBhQDMAgMGBH+BxAYGBABWAgMDAj+lBkkJBkCIhkjDAj+cQYRBgFrBgYGEQb+lQYCUgwI/pQQGBgQAfkRGAwICAwjGf3eGSMjGQGACAzFBgYBbQYRBgYG/pMGEQAAAAIAAACOBAADwAAmAEYAAAEhJy4BByMiBhURFzMeATMhNSEHETQ2MzcXHgEzITIWHQEzNTQmIxMjNTQmIyIGHQEjIgYVFBY7ARUUFjMyNj0BMzI2NTQmA7L93CsNHAXnIS0GFAgTCgH9/hINCAXjKwUYEgInBgdBLSEqjhUODxWODxUVD44VDw4Vjg8VFQN0NBAJAS4g/VAoBgZCAQKjBQcBNQgPBwXLyyAt/hOODxUVD44VDg8Vjg8VFQ+OFQ8OFQAAAAABAIAAQAN/Az0AIAAAASERNCYjIgYVESEiBhUGFjMhERQWMzI2NREhMjY1NCYjA1/+wxMNDRP+vg4SARMNAUMTDQ0TAT0NExIOAd8BPg0TEw3+whMNDRP+wQ4SEg4BPxIODRMABQBAAAADwAOAABIAJABEAE8AbQAAJSImNRE0NjMyFhUROAExFAYjMSMiJjURNDYzMhYVETgBMRQGIwEjNTQmIyEiBh0BIyIGFRQWMyE4ATEyNjU4ATkBNCYjJTQ2MyEyFh0BITUBISImNRE0NjMyFhURFBYzITI2NRE0NjMyFhURFAYCYA0TEw0NExMNwA0TEw0NExMNAgCgOCf+vyg4oA0TEw0DQA0TEw39oBMNAUENEv6AAaD+QCg4Ew0NExMNAcAOEhMNDhI4wBMNAWANExMN/qANExMNAWANExMN/qANEwIgQCg4OChAEw0NExMNDRNADRMTDUBA/OA4KAHgDhISDv4gDRMTDQHfDRMTDf4hKDgAAwC/AEADPwNAAAUACAAPAAABIREhEScfASMBESEVMxEhAm3+UgKA0hNSUv5/AUDA/gADQP0AAi3TbVP+AQJ/wP5BAAADAEcARwOyAzYAFQAaAB8AAAEhJy4BKwEiBhURFBYzITI2NRE0JiMhNTMXIRUhESERA4j+aI0GEgrPEhkZEgMWERkZEfz0wGL+3gMB/P8Cr3YICRkS/WYSGBgSAhQRGVJSNf4CAf4AAAEAAAAAAACAf7rTXw889QALBAAAAAAA1Zhy8wAAAADVmHLzAAAAAAQAA8AAAAAIAAIAAAAAAAAAAQAAA8D/wAAABAAAAAAABAAAAQAAAAAAAAAAAAAAAAAAAAwEAAAAAAAAAAAAAAACAAAABAABMgQAANsEAACpBAAAAAQAAIAEAABABAAAvwQAAEcAAAAAAAoAFAAeAEIAZgC2ARgBSgHUAfYCKgAAAAEAAAAMAG4ABQAAAAAAAgAAAAAAAAAAAAAAAAAAAAAAAAAOAK4AAQAAAAAAAQAHAAAAAQAAAAAAAgAHAGAAAQAAAAAAAwAHADYAAQAAAAAABAAHAHUAAQAAAAAABQALABUAAQAAAAAABgAHAEsAAQAAAAAACgAaAIoAAwABBAkAAQAOAAcAAwABBAkAAgAOAGcAAwABBAkAAwAOAD0AAwABBAkABAAOAHwAAwABBAkABQAWACAAAwABBAkABgAOAFIAAwABBAkACgA0AKRpY29tb29uAGkAYwBvAG0AbwBvAG5WZXJzaW9uIDEuMABWAGUAcgBzAGkAbwBuACAAMQAuADBpY29tb29uAGkAYwBvAG0AbwBvAG5pY29tb29uAGkAYwBvAG0AbwBvAG5SZWd1bGFyAFIAZQBnAHUAbABhAHJpY29tb29uAGkAYwBvAG0AbwBvAG5Gb250IGdlbmVyYXRlZCBieSBJY29Nb29uLgBGAG8AbgB0ACAAZwBlAG4AZQByAGEAdABlAGQAIABiAHkAIABJAGMAbwBNAG8AbwBuAC4AAAADAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
    }, function(A, e) {
        A.exports = "data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBzdGFuZGFsb25lPSJubyI/Pg0KPCFET0NUWVBFIHN2ZyBQVUJMSUMgIi0vL1czQy8vRFREIFNWRyAxLjEvL0VOIiAiaHR0cDovL3d3dy53My5vcmcvR3JhcGhpY3MvU1ZHLzEuMS9EVEQvc3ZnMTEuZHRkIiA+DQo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+DQo8bWV0YWRhdGE+R2VuZXJhdGVkIGJ5IEljb01vb248L21ldGFkYXRhPg0KPGRlZnM+DQo8Zm9udCBpZD0iaWNvbW9vbiIgaG9yaXotYWR2LXg9IjEwMjQiPg0KPGZvbnQtZmFjZSB1bml0cy1wZXItZW09IjEwMjQiIGFzY2VudD0iOTYwIiBkZXNjZW50PSItNjQiIC8+DQo8bWlzc2luZy1nbHlwaCBob3Jpei1hZHYteD0iMTAyNCIgLz4NCjxnbHlwaCB1bmljb2RlPSImI3gyMDsiIGhvcml6LWFkdi14PSI1MTIiIGQ9IiIgLz4NCjxnbHlwaCB1bmljb2RlPSImI3hlOTAwOyIgZ2x5cGgtbmFtZT0iY2FyZXQtZG93biIgZD0iTTYzNS4zMTEgNDQ4cTAtMTQuODQ4LTEwLjg0OC0yNS43MjhsLTI1Ni0yNTZxLTEwLjg0OC0xMC44NDgtMjUuNzI4LTEwLjg0OHQtMjUuNzI4IDEwLjg0OC0xMC44NDggMjUuNzI4djUxMnEwIDE0Ljg0OCAxMC44NDggMjUuNzI4dDI1LjcyOCAxMC44NDggMjUuNzI4LTEwLjg0OGwyNTYtMjU2cTEwLjg0OC0xMC44NDggMTAuODQ4LTI1LjcyOHoiIC8+DQo8Z2x5cGggdW5pY29kZT0iJiN4ZTkwMTsiIGdseXBoLW5hbWU9ImNhcmV0LXJpZ2h0IiBkPSJNNTEyIDMwMS43MTRxLTE0Ljg0OCAwLTI1Ljc0NiAxMC44MjVsLTI1NiAyNTZxLTEwLjgyNSAxMC44MjUtMTAuODI1IDI1Ljc0NnQxMC44MjUgMjUuNzQ2IDI1Ljc0NiAxMC44MjVoNTEycTE0Ljg0OCAwIDI1Ljc0Ni0xMC44MjV0MTAuODI1LTI1Ljc0Ni0xMC44MjUtMjUuNzQ2bC0yNTYtMjU2cS0xMC44MjUtMTAuODI1LTI1Ljc0Ni0xMC44MjV6IiAvPg0KPGdseXBoIHVuaWNvZGU9IiYjeGU5MDI7IiBnbHlwaC1uYW1lPSJlZGl0IiBkPSJNODE2LjAwMSA1OTQuMjU2Yy0xMS4xNTEgMC0yMC4yMDMtOS4wNTMtMjAuMjAzLTIwLjIwMXYtMzYzLjY2NWMwLTIyLjMxLTE4LjA4OS00MC40MDItNDAuNDAxLTQwLjQwMmgtNTA1LjA4M2MtMjIuMzE4IDAtNDAuNDA1IDE4LjA5My00MC40MDUgNDAuNDAydjUwNS4wODZjMCAyMi4zMTcgMTguMDk0IDQwLjQwMSA0MC40MDUgNDAuNDAxaDM0My40NmMxMS4xNjggMCAyMC4yIDkuMDM4IDIwLjIgMjAuMjA0IDAgMTEuMTYxLTkuMDM2IDIwLjIwMS0yMC4yIDIwLjIwMWgtMzYzLjY3NWMtMzMuNDgzIDAtNjAuNjEyLTI3LjEzNi02MC42MTItNjAuNjF2LTU0NS40OTRjMC0zMy40ODEgMjcuMTMtNjAuNjEyIDYwLjYxMi02MC42MTJoNTQ1LjQ5NGMzMy40ODIgMCA2MC42MTMgMjcuMTMzIDYwLjYxMyA2MC42MTJ2MzgzLjg3N2MtMC4wMDQgMTEuMTQ4LTkuMDQwIDIwLjIwMS0yMC4yMDUgMjAuMjAxek00MTcuMTU4IDM5Ny4xOTRjNy44OTMtNy44NjggMjAuNjc0LTcuODY4IDI4LjU3MiAwbDM2My4wNjggMzY1LjA0OWM3Ljg4OSA3Ljg5NCA3Ljg4OSAyMC42ODkgMCAyOC41NzItNy44OSA3Ljg5My0yMC42NzggNy44OTMtMjguNTYzIDBsLTM2My4wNzctMzY1LjAzNWMtNy44OTctNy44OTEtNy44OTctMjAuNjkyIDAtMjguNTg2eiIgLz4NCjxnbHlwaCB1bmljb2RlPSImI3hlOTAzOyIgZ2x5cGgtbmFtZT0iZm9sZGVyLXBsdXMtZSIgZD0iTTk0Ni40NzEgODg0LjE2OWgtNTQ4LjQyNmwtNDIuODU2IDUxLjkxOGMtMTcuNDk4IDIxLjM5NC0zOS41MjUgMjUuMS00Ni42NzYgMjMuNjI5aC0yMzAuOTIyYy00Mi43ODggMC03Ny41OTEtMzQuODAzLTc3LjU5MS03Ny41MjV2LTY4OC4yODRsNi4zMjctMzkuNzg0aDE5Ljg1N2MxMC40NjctNy42MDMgMjMuMTIxLTExLjc1MSAzNi40NjktMTEuNzUxaDUwOS4zN3Y2NS4zOTJoLTQ5NC40MzNsLTEyLjE5NC0xLjAyMHY2NzUuNDQ4YzAgNi43MDQgNS40ODYgMTIuMTMzIDEyLjE5NCAxMi4xMzNsMjI3LjAyNSAwLjI1NiA0My41NTEtNTIuMTA4YzYuOTYtMTEuNDMgMjIuNzk5LTIzLjY5MyA0Ni40MjQtMjMuNjkzaDU1MS44OGM2LjcwOSAwIDEyLjEzMy01LjQ5IDEyLjEzMy0xMi4xOTh2LTIwMi4xOWg2NS4zOTd2MjAyLjE4OWMwIDQyLjc4Ny0zNC44MDMgNzcuNTg5LTc3LjUyOSA3Ny41ODl6TTk4OC40NTEgMzkxLjIxNWgtMTQyLjE5N3YxNDIuMTk3YzAgMTkuNjUtMTUuOSAzNS41NDktMzUuNTQ5IDM1LjU0OXMtMzUuNTQ5LTE1LjktMzUuNTQ5LTM1LjU0OXYtMTQyLjE5N2gtMTQyLjE5N2MtMTkuNjUgMC0zNS41NDktMTUuOS0zNS41NDktMzUuNTQ5czE1LjktMzUuNTQ5IDM1LjU0OS0zNS41NDloMTQyLjE5N3YtMTQyLjE5N2MwLTE5LjY1IDE1LjktMzUuNTQ5IDM1LjU0OS0zNS41NDlzMzUuNTQ5IDE1LjkgMzUuNTQ5IDM1LjU0OXYxNDIuMTk3aDE0Mi4xOTdjMTkuNjUgMCAzNS41NDkgMTUuOSAzNS41NDkgMzUuNTQ5cy0xNS45IDM1LjU0OS0zNS41NDkgMzUuNTQ5eiIgLz4NCjxnbHlwaCB1bmljb2RlPSImI3hlOTA0OyIgZ2x5cGgtbmFtZT0icGx1cyIgZD0iTTg2My4zMjggNDc4LjY2bC0zMTcuMzQ0LTAuMXYzMTguNjIyYzAgMTcuNjY1LTE0LjMzNiAzMi4wMDEtMzIgMzIuMDAxcy0zMi0xNC4zMzYtMzItMzJ2LTMxOC40MDFsLTMyMi40NjUgMC4xNzdjLTE3LjYzMiAwLTMxLjkzNS0xNC4yNC0zMi0zMS45MDQtMC4wOTctMTcuNjY1IDE0LjIwOC0zMi4wMzIgMzEuODcxLTMyLjA5NmwzMjIuNTkzLTAuMTc3di0zMTkuMTY3YzAtMTcuNjk2IDE0LjMzNi0zMi4wMDEgMzEuOTk5LTMyLjAwMXMzMiAxNC4zMDMgMzIgMzJ2MzE4Ljk0NmwzMTcuMjE2IDAuMWMxNy42MzIgMCAzMS45MzUgMTQuMjQgMzIgMzEuOTA1cy0xNC4yMzggMzIuMDMxLTMxLjg3IDMyLjA5NXoiIC8+DQo8Z2x5cGggdW5pY29kZT0iJiN4ZTkwNTsiIGdseXBoLW5hbWU9InRyYXNoIiBkPSJNNjA3LjkwNCAxOTEuOTUyYy0xNy43MTIgMC0zMiAxNC4yODgtMzIgMzJ2MzUyLjExMmMwIDE3LjcxMiAxNC4yODggMzIgMzIgMzJzMzEuOTg0LTE0LjI4OCAzMS45ODQtMzJ2LTM1MS45MzZjMC0wLjAzMyAwLTAuMDczIDAtMC4xMTIgMC0xNy42OC0xNC4zMS0zMi4wMTktMzEuOTgtMzIuMDY0aC0wLjAwNHpNNDE1LjkzNiAxOTEuOTUyYy0xNy43MTIgMC0zMiAxNC4yODgtMzIgMzJ2MzUyLjExMmMwIDE3LjcxMiAxNC4yNzIgMzIgMzIgMzJzMzItMTQuMjg4IDMyLTMydi0zNTEuOTM2YzAtMC4wMjQgMC0wLjA1MiAwLTAuMDgwIDAtMTcuNjkyLTE0LjMxNS0zMi4wNDEtMzEuOTk1LTMyLjA5NmgtMC4wMDV6TTkyOC4wMTYgNzM2LjAzMmgtMTU5Ljk2OHY2My45ODRjMCA1Mi45OTItNDIuNjcyIDk1Ljk4NC05NS4yOTYgOTUuOTg0aC0zMjAuODE2Yy01Mi45OTktMC4wMjctOTUuOTU3LTQyLjk4NC05NS45ODQtOTUuOTgxdi02My45ODdoLTE1OS45NjhjLTE3LjcxMiAwLTMyLTE0LjI4OC0zMi0zMnMxNC4yNzItMzIgMzItMzJoODMyLjAzMmMwLjAxNCAwIDAuMDMxIDAgMC4wNDggMCAxNy42MzggMCAzMS45MzYgMTQuMjk4IDMxLjkzNiAzMS45MzYgMCAwLjAyMyAwIDAuMDQ1IDAgMC4wNjh2LTAuMDA0YzAgMTcuNzI4LTE0LjI3MiAzMi0zMS45ODQgMzJ6TTMxOS45NTIgODAwLjAxNmMwIDE3LjU1MiAxNC40NDggMzIgMzIgMzJoMzIwLjgxNmMxNy41MzYgMCAzMS4zMTItMTQuMTEyIDMxLjMxMi0zMnYtNjMuOTg0aC0zODQuMTI4djYzLjk4NHpNNzM2LjA0OCAwaC00NDcuOTJjLTUzLjAwMSAwLjAwOS05NS45NjYgNDIuOTY4LTk1Ljk4NCA5NS45NjZ2NDgwLjQzNGMwIDE3LjcxMiAxNC4yNzIgMzIgMzIgMzJzMzItMTQuMjg4IDMyLTMydi00ODAuNDMyYzAtMTcuNzEyIDE0LjQ0OC0zMiAzMi0zMmg0NDguMDk2YzE3LjcxMiAwIDMyIDE0LjI4OCAzMiAzMnY0NzkuMjMyYzAgMTcuNzEyIDE0LjI4OCAzMiAzMiAzMnMzMS45ODQtMTQuMjg4IDMxLjk4NC0zMnYtNDc5LjIzMmMtMC4xOTItNTIuODE2LTQzLjItOTUuOTY4LTk2LjE3Ni05NS45Njh6IiAvPg0KPGdseXBoIHVuaWNvZGU9IiYjeGU5MDY7IiBnbHlwaC1uYW1lPSJmaWxlIiBkPSJNNjIwLjc5MiA4MzIuMjE4aC00MzAuMDc2di03NjcuNzUzaDY0MC41MjF2NTU2LjkzM2wtMjEwLjQ0NSAyMTAuODE5ek02MzkuNTM4IDcyMi43NjVsODIuNDQxLTgyLjU4OGgtODIuNDQxdjgyLjU4OHpNMjU0Ljc3NSAxMjguNTI0djYzOS42MzVoMzIwLjcwNHYtMTkyLjA0MWgxOTEuNjk5di00NDcuNTk1aC01MTIuNDAzeiIgLz4NCjxnbHlwaCB1bmljb2RlPSImI3hlOTA3OyIgZ2x5cGgtbmFtZT0iZm9sZGVyIiBkPSJNOTAzLjgxMyA2ODcuMjA2aC00MDcuOTE4bC0xNDEuMzQyIDExOC4wOTVjLTcuOTQ2IDEwLjUxLTIwLjQ3NiAxNi43NzQtMzMuNTIxIDE2Ljc3NGgtMjA3LjUwNGMtMjMuMjcxIDAtNDIuMjIzLTE5LjE1OC00Mi4yMjMtNDIuNzE0di02NjYuNTk5YzAtMjMuMjk5IDE4Ljk1LTQyLjI1MSA0Mi4yMjMtNDIuMjUxaDc5MC4yODZjMjMuMjk2IDAgNDIuMjQ2IDE4Ljk1IDQyLjI0NiA0Mi4yNTF2NTMyLjIyM2MwLjAwMSAyMy4yNzQtMTguOTQ5IDQyLjIyMy00Mi4yNDUgNDIuMjIzek0xMjQuMzIgNjg3LjIwNnY4MS44NTJoMTkxLjg2OWw5Ny40MzUtODEuODUyaC0yODkuMzA1ek0xMjQuMzIgNjM0LjE4N2g3NjguNzJ2LTUxMC42NTdoLTc2OC43MnY1MTAuNjU3eiIgLz4NCjwvZm9udD48L2RlZnM+PC9zdmc+"
    }, function(A, e, t) {
        function M(A) {
            for (var e = 0; e < A.length; e++) {
                var t = A[e],
                    M = d[t.id];
                if (M) {
                    M.refs++;
                    for (var i = 0; i < M.parts.length; i++) M.parts[i](t.parts[i]);
                    for (; i < t.parts.length; i++) M.parts.push(n(t.parts[i]));
                    M.parts.length > t.parts.length && (M.parts.length = t.parts.length)
                } else {
                    for (var o = [], i = 0; i < t.parts.length; i++) o.push(n(t.parts[i]));
                    d[t.id] = {
                        id: t.id,
                        refs: 1,
                        parts: o
                    }
                }
            }
        }

        function i() {
            var A = document.createElement("style");
            return A.type = "text/css", N.appendChild(A), A
        }

        function n(A) {
            var e, t, M = document.querySelector('style[data-vue-ssr-id~="' + A.id + '"]');
            if (M) {
                if (I) return c;
                M.parentNode.removeChild(M)
            }
            if (u) {
                var n = s++;
                M = E || (E = i()), e = o.bind(null, M, n, !1), t = o.bind(null, M, n, !0)
            } else M = i(), e = r.bind(null, M), t = function() {
                M.parentNode.removeChild(M)
            };
            return e(A),
                function(M) {
                    if (M) {
                        if (M.css === A.css && M.media === A.media && M.sourceMap === A.sourceMap) return;
                        e(A = M)
                    } else t()
                }
        }

        function o(A, e, t, M) {
            var i = t ? "" : M.css;
            if (A.styleSheet) A.styleSheet.cssText = D(e, i);
            else {
                var n = document.createTextNode(i),
                    o = A.childNodes;
                o[e] && A.removeChild(o[e]), o.length ? A.insertBefore(n, o[e]) : A.appendChild(n)
            }
        }

        function r(A, e) {
            var t = e.css,
                M = e.media,
                i = e.sourceMap;
            if (M && A.setAttribute("media", M), i && (t += "\n/*# sourceURL=" + i.sources[0] + " */", t += "\n/*# sourceMappingURL=data:application/json;base64," + btoa(unescape(encodeURIComponent(JSON.stringify(i)))) + " */"), A.styleSheet) A.styleSheet.cssText = t;
            else {
                for (; A.firstChild;) A.removeChild(A.firstChild);
                A.appendChild(document.createTextNode(t))
            }
        }
        var a = "undefined" != typeof document,
            g = t(10),
            d = {},
            N = a && (document.head || document.getElementsByTagName("head")[0]),
            E = null,
            s = 0,
            I = !1,
            c = function() {},
            u = "undefined" != typeof navigator && /msie [6-9]\b/.test(navigator.userAgent.toLowerCase());
        A.exports = function(A, e, t) {
            I = t;
            var i = g(A, e);
            return M(i),
                function(e) {
                    for (var t = [], n = 0; n < i.length; n++) {
                        var o = i[n],
                            r = d[o.id];
                        r.refs--, t.push(r)
                    }
                    e ? (i = g(A, e), M(i)) : i = [];
                    for (var n = 0; n < t.length; n++) {
                        var r = t[n];
                        if (0 === r.refs) {
                            for (var a = 0; a < r.parts.length; a++) r.parts[a]();
                            delete d[r.id]
                        }
                    }
                }
        };
        var D = function() {
            var A = [];
            return function(e, t) {
                return A[e] = t, A.filter(Boolean).join("\n")
            }
        }()
    }, function(A, e) {
        A.exports = function(A, e) {
            for (var t = [], M = {}, i = 0; i < e.length; i++) {
                var n = e[i],
                    o = n[0],
                    r = n[1],
                    a = n[2],
                    g = n[3],
                    d = {
                        id: A + ":" + i,
                        css: r,
                        media: a,
                        sourceMap: g
                    };
                M[o] ? M[o].parts.push(d) : t.push(M[o] = {
                    id: o,
                    parts: [d]
                })
            }
            return t
        }
    }, function(A, e) {
        A.exports = function(A, e, t, M) {
            var i, n = A = A || {},
                o = typeof A.default;
            "object" !== o && "function" !== o || (i = A, n = A.default);
            var r = "function" == typeof n ? n.options : n;
            if (e && (r.render = e.render, r.staticRenderFns = e.staticRenderFns), t && (r._scopeId = t), M) {
                var a = Object.create(r.computed || null);
                Object.keys(M).forEach(function(A) {
                    var e = M[A];
                    a[A] = function() {
                        return e
                    }
                }), r.computed = a
            }
            return {
                esModule: i,
                exports: n,
                options: r
            }
        }
    }, function(A, e, t) {
        "use strict";
        Object.defineProperty(e, "__esModule", {
            value: !0
        });
        var M = t(13),
            i = t(14),
            n = null;
        e.default = {
            data: function() {
                return {
                    isHover: !1,
                    editable: !1,
                    isDragEnterUp: !1,
                    isDragEnterBottom: !1,
                    isDragEnterNode: !1,
                    expanded: !0
                }
            },
            props: {
                model: {
                    type: Object
                },
                defaultLeafNodeName: {
                    type: String,
                    default: "New leaf node"
                },
                defaultTreeNodeName: {
                    type: String,
                    default: "New tree node"
                }
            },
            computed: {
                itemIconClass: function() {
                    return this.model.isLeaf ? "icon-file" : "icon-folder"
                },
                caretClass: function() {
                    return this.expanded ? "icon-caret-down" : "icon-caret-right"
                },
                isFolder: function() {
                    return this.model.children && this.model.children.length
                }
            },
            mounted: function() {
                var A = this;
                (0, i.addHandler)(window, "keyup", function(e) {
                    13 === e.keyCode && A.editable && (A.editable = !1)
                })
            },
            beforeDestroy: function() {
                (0, i.removeHandler)(window, "keyup")
            },
            methods: {
                updateName: function(A) {
                    this.model.changeName(A.target.value)
                },
                delNode: function() {
                    var A = this;
                    window.confirm("¿Estas seguro?") && A.model.remove()
                },
                setEditable: function() {
                    var A = this;
                    this.editable = !0, this.$nextTick(function() {
                        A.$refs.nodeInput.focus()
                    })
                },
                setUnEditable: function() {
                    this.editable = !1
                },
                toggle: function() {
                    this.isFolder && (this.expanded = !this.expanded)
                },
                mouseOver: function(A) {
                    this.isHover = !0
                },
                mouseOut: function(A) {
                    this.isHover = !1
                },
                click: function() {
                    for (var A = this.$parent, e = this.model;
                        "root" !== A._props.model.name;) A = A.$parent;
                    A.$emit("click", e)
                },
                addChild: function(A) {
                    var e = A ? this.defaultLeafNodeName : this.defaultTreeNodeName;
                    this.expanded = !0;
                    var t = new M.TreeNode({
                        name: e,
                        isLeaf: A
                    });
                    this.model.addChildren(t, !0)
                },
                dragStart: function(A) {
                    return !this.model.dragDisabled && (n = this, A.dataTransfer.setData("data", "data"), A.dataTransfer.effectAllowed = "move", !0)
                },
                dragEnd: function(A) {
                    n = null
                },
                dragOver: function(A) {
                    return A.preventDefault(), !0
                },
                dragEnter: function(A) {
                    n && (this.model.isLeaf || (this.isDragEnterNode = !0))
                },
                dragLeave: function(A) {
                    this.isDragEnterNode = !1
                },
                drop: function(A) {
                    n && (n.model.moveInto(this.model), this.isDragEnterNode = !1)
                },
                dragEnterUp: function() {
                    n && (this.isDragEnterUp = !0)
                },
                dragOverUp: function(A) {
                    return A.preventDefault(), !0
                },
                dragLeaveUp: function() {
                    n && (this.isDragEnterUp = !1)
                },
                dropUp: function() {
                    n && (n.model.insertBefore(this.model), this.isDragEnterUp = !1)
                },
                dragEnterBottom: function() {
                    n && (this.isDragEnterBottom = !0)
                },
                dragOverBottom: function(A) {
                    return A.preventDefault(), !0
                },
                dragLeaveBottom: function() {
                    n && (this.isDragEnterBottom = !1)
                },
                dropBottom: function() {
                    n && (n.model.insertAfter(this.model), this.isDragEnterBottom = !1)
                }
            },
            beforeCreate: function() {
                this.$options.components.item = t(1)
            }
        }
    }, function(A, e) {
        "use strict";

        function t(A) {
            return this.root = new i({
                name: "root",
                isLeaf: !1,
                id: 0
            }), this.initNode(this.root, A), this.root
        }
        var M = 1,
            i = function(A) {
                var e = A.id,
                    t = A.isLeaf;
                this.id = "undefined" == typeof e ? "new" + M++ : e, this.parent = null, this.children = null, this.isLeaf = !!t;
                for (var i in A) "id" !== i && "children" !== i && "isLeaf" !== i && (this[i] = A[i])
            };
        i.prototype.changeName = function(A) {
            this.name = A
        }, i.prototype.addChildren = function(A, e) {
            if (this.children || (this.children = []), Array.isArray(A)) {
                for (var t = 0, M = A.length; t < M; t++) {
                    var i = A[t];
                    i.parent = this, i.pid = this.id
                }
                this.children.concat(A)
            } else {
                var n = A;
                n.parent = this, n.pid = this.id, this.children.push(n)
            }
        }, i.prototype.remove = function() {
            var A = this.parent,
                e = A.findChildIndex(this);
            A.children.splice(e, 1)
        }, i.prototype._removeChild = function(A) {
            for (var e = 0, t = this.children.length; e < t; e++)
                if (this.children[e] === A) {
                    this.children.splice(e, 1);
                    break
                }
        }, i.prototype.isTargetChild = function(A) {
            for (var e = A.parent; e;) {
                if (e === this) return !0;
                e = e.parent
            }
            return !1
        }, i.prototype.moveInto = function(A) {
            "root" !== this.name && this !== A && (this.isTargetChild(A) || A.isLeaf || (this.parent._removeChild(this), this.parent = A, this.pid = A.id, A.children || (A.children = []), A.children.unshift(this)))
        }, i.prototype.findChildIndex = function(A) {
            for (var e, t = 0, M = this.children.length; t < M; t++)
                if (this.children[t] === A) {
                    e = t;
                    break
                } return e
        }, i.prototype._beforeInsert = function(A) {
            return "root" !== this.name && this !== A && (!this.isTargetChild(A) && (this.parent._removeChild(this), this.parent = A.parent, this.pid = A.parent.id, !0))
        }, i.prototype.insertBefore = function(A) {
            if (this._beforeInsert(A)) {
                var e = A.parent.findChildIndex(A);
                A.parent.children.splice(e, 0, this)
            }
        }, i.prototype.insertAfter = function(A) {
            if (this._beforeInsert(A)) {
                var e = A.parent.findChildIndex(A);
                A.parent.children.splice(e + 1, 0, this)
            }
        }, t.prototype.initNode = function(A, e) {
            for (var t = 0, M = e.length; t < M; t++) {
                var n = e[t],
                    o = new i(n);
                n.children && n.children.length > 0 && this.initNode(o, n.children), A.addChildren(o)
            }
        }, e.Tree = t, e.TreeNode = i
    }, function(A, e) {
        "use strict";
        var t;
        e.addHandler = function(A, e, M) {
            t = M, A.addEventListener ? A.addEventListener(e, M, !1) : A.attachEvent ? A.attachEvent("on" + e, M) : A["on" + e] = M
        }, e.removeHandler = function(A, e) {
            A.removeEventListener ? A.removeEventListener(e, t, !1) : A.detachEvent ? A.detachEvent("on" + e, t) : A["on" + e] = null
        }
    }, function(A, e) {
        A.exports = {
            render: function() {
                var A = this,
                    e = A.$createElement,
                    t = A._self._c || e;
                return t("div", ["root" !== A.model.name ? t("div", [t("div", {
                    staticClass: "border up",
                    class: {
                        active: A.isDragEnterUp
                    },
                    on: {
                        drop: A.dropUp,
                        dragenter: A.dragEnterUp,
                        dragover: A.dragOverUp,
                        dragleave: A.dragLeaveUp
                    }
                }), A._v(" "), t("div", {
                    staticClass: "tree-node",
                    class: {
                        active: A.isDragEnterNode
                    },
                    attrs: {
                        id: A.model.id,
                        draggable: !A.model.dragDisabled
                    },
                    on: {
                        dragstart: A.dragStart,
                        dragover: A.dragOver,
                        dragenter: A.dragEnter,
                        dragleave: A.dragLeave,
                        drop: A.drop,
                        dragend: A.dragEnd,
                        mouseover: A.mouseOver,
                        mouseout: A.mouseOut,
                        click: function(e) {
                            e.stopPropagation(), A.click(e)
                        }
                    }
                }, [A.model.children && A.model.children.length > 0 ? t("span", {
                    staticClass: "caret icon is-small"
                }, [t("i", {
                    staticClass: "vue-tree-icon",
                    class: A.caretClass,
                    on: {
                        click: function(e) {
                            e.preventDefault(), e.stopPropagation(), A.toggle(e)
                        }
                    }
                })]) : A._e(), A._v(" "), A.model.isLeaf ? t("span", [A._t("leafNodeIcon", [t("i", {
                    staticClass: "vue-tree-icon item-icon icon-file"
                })])], 2) : t("span", [A._t("treeNodeIcon", [t("i", {
                    staticClass: "vue-tree-icon item-icon icon-folder"
                })])], 2), A._v(" "), A.editable ? t("input", {
                    ref: "nodeInput",
                    staticClass: "vue-tree-input",
                    attrs: {
                        type: "text"
                    },
                    domProps: {
                        value: A.model.name
                    },
                    on: {
                        input: A.updateName,
                        blur: A.setUnEditable
                    }
                }) : t("div", {
                    staticClass: "node-content"
                }, [A._v("\n        " + A._s(A.model.name) + "\n      ")]), A._v(" "), t("div", {
                    directives: [{
                        name: "show",
                        rawName: "v-show",
                        value: A.isHover,
                        expression: "isHover"
                    }],
                    staticClass: "operation"
                }, [A.model.isLeaf ? A._e() : t("span", {
                    attrs: {
                        title: "add tree node"
                    },
                    on: {
                        click: function(e) {
                            e.stopPropagation(), e.preventDefault(), A.addChild(!1)
                        }
                    }
                }, [A._t("addTreeNode", [t("i", {
                    staticClass: "vue-tree-icon icon-folder-plus-e"
                })])], 2), A._v(" "), A.model.isLeaf ? A._e() : t("span", {
                    attrs: {
                        title: "add tree node"
                    },
                    on: {
                        click: function(e) {
                            e.stopPropagation(), e.preventDefault(), A.addChild(!0)
                        }
                    }
                }, [A._t("addLeafNode", [t("i", {
                    staticClass: "vue-tree-icon icon-plus"
                })])], 2), A._v(" "), t("span", {
                    attrs: {
                        title: "edit"
                    },
                    on: {
                        click: function(e) {
                            e.stopPropagation(), e.preventDefault(), A.setEditable(e)
                        }
                    }
                }, [A._t("edit", [t("i", {
                    staticClass: "vue-tree-icon icon-edit"
                })])], 2), A._v(" "), t("span", {
                    attrs: {
                        title: "delete"
                    },
                    on: {
                        click: function(e) {
                            e.stopPropagation(), e.preventDefault(), A.delNode(e)
                        }
                    }
                }, [A._t("edit", [t("i", {
                    staticClass: "vue-tree-icon icon-trash"
                })])], 2)])]), A._v(" "), A.model.children && A.model.children.length > 0 && A.expanded ? t("div", {
                    staticClass: "border bottom",
                    class: {
                        active: A.isDragEnterBottom
                    },
                    on: {
                        drop: A.dropBottom,
                        dragenter: A.dragEnterBottom,
                        dragover: A.dragOverBottom,
                        dragleave: A.dragLeaveBottom
                    }
                }) : A._e()]) : A._e(), A._v(" "), A.isFolder ? t("div", {
                    directives: [{
                        name: "show",
                        rawName: "v-show",
                        value: A.expanded,
                        expression: "expanded"
                    }],
                    class: {
                        "tree-margin": "root" !== A.model.name
                    }
                }, A._l(A.model.children, function(e) {
                    return t("item", {
                        key: e.id,
                        attrs: {
                            "default-tree-node-name": A.defaultTreeNodeName,
                            "default-leaf-node-name": A.defaultLeafNodeName,
                            model: e
                        }
                    })
                })) : A._e()])
            },
            staticRenderFns: []
        }
    }])
});