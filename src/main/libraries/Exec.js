// libreria para llamado de procesos externos
const { spawn, exec } = require("child_process");
// libreria para manejo de promesas
const Q = require("q");
// libreria para manejo del sistema operativo
var os = require("os");
// manejo de rutas
var path = require('path');
// manejo de arrays
var _ = require('lodash')

var bridge = {
  /*
	* Recibe una instrucción y los parametros y retorna la respuesta
	* Santiago Blanco
	*/
  executeCommand: info => {

      if (!_.isNil(info.RPath)) {
        var commandLine = info.RPath + 'Rscript'
      } else {
        var commandLine = 'Rscript'
      }
      console.log("commandLine", commandLine)
    // return spawn(commandLine, [path.resolve("./scriptsxm/main.R")]);
    return spawn(path.resolve("./R/bin/") + '/Rscript', [path.resolve("./scriptsxm/main.R")]);
  },
  /*
	* Ejecuta una instrucción y espera la salida
	* Santiago Blanco
	*/
  execCommand: (command, args, fn) => {
    return spawn(command, args, fn);
  },
  /*
	* Permite matar el proceso que ejecuta R
	* Santiago Blanco
	*/
  kill(pid) {
    if (os.platform() === "win32") {
      console.log("Estamos en windows");
    }

    if (os.platform() === "darwin") {
      return new Promise((resolve, reject) => {
        exec("kill", pid, v => {
          resolve(v);
        });
      });
    }
  },
  /*
	* Captura la ruta de instalación de R
	* Santiago Blanco
	*/
  getRPath(info) {
    return new Promise((resolve, reject) => {
      if (!_.isNil(info.RPath)) {
        var commandLine = info.RPath + 'script'
      } else {
        var commandLine = 'Rscript'
      }
      var command = bridge.execCommand(commandLine, ["path.R"]);
      command.stdout.on("data", data => {
        var pth = data.toString("utf8")
        resolve(pth.replace('[1] "', '').replace(/"/g, "").trim());
      });
      command.stderr.on("data", function(data) {
        if (data.toString()) {
          reject(data.toString("utf8"));
        }
      });
    });
  },
  /*
	* Captura la versión de R.
	* Santiago Blanco
	*/
  getRVersion(info) {
    return new Promise((resolve, reject) => {

      if (!_.isNil(info.RPath)) {
        var commandLine = info.RPath + 'script'
      } else {
        var commandLine = 'Rscript'
      }

      var command = bridge.execCommand(commandLine, ["--version"]);
      
      command.stdout.on("data", v => {
        var RvrsionText = v.toString();
        var pattern = new RegExp("[0-9].[0-9].[0-9]");
        var regObj = pattern.exec(RvrsionText);
        var version = regObj[0];
        resolve(version);
      });

      command.stderr.on("data", v => {
        var RvrsionText = v.toString();
        var pattern = new RegExp("[0-9].[0-9].[0-9]");
        var regObj = pattern.exec(RvrsionText);
        var version = regObj[0];
        resolve(version);
      });
    });
  }
};

export default bridge;
