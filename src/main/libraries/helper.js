// manejo de procesos asincronos
import async from 'async'
// manejo de promesas
import Q from 'q'

const json2csv = require('json2csv').Parser;
const csvtojson = require("csvtojson/v2");
const fs = require('fs')
const configCSVPath = './scriptsxm/parameters.csv'
const path = require("path")

var archiver = require('archiver');

export default {


  comprime ()  {
    return new Promise((resolve, reject) => {
      var output = fs.createWriteStream(path.resolve("./scriptsxm/resultados.zip"));
      var archive = archiver('zip');

      output.on('close', function () {
          console.log(archive.pointer() + ' total bytes');
          console.log('archiver has been finalized and the output file descriptor has closed.');
          resolve()
      });

      archive.on('error', function(err){
          console.log(err)
          throw err;
      });

        archive.pipe(output);

        archive.append(fs.createReadStream(path.resolve("./scriptsxm/RES_Corr.csv")), { name: 'RES_Corr.csv' });
        archive.append(fs.createReadStream(path.resolve("./scriptsxm/RES_Durac.csv")), { name: 'RES_Durac.csv' });
        archive.append(fs.createReadStream(path.resolve("./scriptsxm/RES_Mape.csv")), { name: 'RES_Mape.csv' });
        archive.append(fs.createReadStream(path.resolve("./scriptsxm/RES_Pron.csv")), { name: 'RES_Pron.csv' });
        archive.append(fs.createReadStream(path.resolve("./scriptsxm/BD_Demanda.csv")), { name: 'BD_Demanda.csv' });

        archive.finalize();
    })

  },





  /*
   * Permite formatear la estructura de los parametros de entrada
   * con sus respectivos valores
   * Santiago Blanco
   */
  formatStructure(inputs) {
    return new Promise((resolve, reject) => {
      var json = {
          escalarDemanda:null,
          rezago_minutos:null,
          rezago_dias:null,
          rezago_minuto_dias:null,
          pronosticar_RL:null,
          pronosticar_GAM:null,
          pronosticar_ARIMA:null,
          pronosticar_ELMAN:null,
          pronosticar_LSTM:null,
          k:null,
          Ar:null,
          I:null,
          Ma:null,
          capasElman:null,
          neuronasCapa1Elman:null,
          neuronasCapa2Elman:null,
          maxitElman:null,
          learnRateElman:null,
          capasLSTM:null,
          neuronasCapa1LSTM:null,
          neuronasCapa2LSTM:null,
          activacionCapa1LSTM:null,
          activacionCapa2LSTM:null,
          batchSizeLSTM:null,
          maxitLSTM:null,
          learnRateLSTM:null,
          nivelConf:null,
          nDatosFilt:null,
          nDatosEnt:null,
          PenFiltrHP:null,
          diasPronostico:null,
          metodo_comb:null,
          nPerAntCorrec:null,
          alphaCorrec:null,
          kClusters:null,
          nDatosClust:null,
          Norma:null,
          filtrarBarras:null,
          Construye_BD:null,
          lambdaBD:null,
          nDiasBloque:null,
          numBloque:null,
          nPasosGrafCorr:null,
          nDiasHistComb: null
      }

      async.eachSeries(inputs, (input, callback) => {
        if (input.name) {

          if (input.value === true) {
            input.value = 'TRUE'
          }

          if (input.value === false) {
            input.value = 'FALSE'
          }


          json[input.name] = input.value
        }
        callback()
      }, () => {
        resolve(json)
      })
    })
  },
  /*
   * Permite agrupar los inputs por tab
   * Santiago Blanco
   */
  getInputs(position, tabs) {
    return new Promise((resolve, reject) => {
      var inputs = []
      async.eachSeries(tabs, (tab, callback) => {
        async.eachSeries(tab.inputs, (input, cb) => {
          inputs.push(input)
          cb()
        }, () => {
          callback()
        })
      }, () => {
        resolve(inputs)
      })
    })
  },
  /*
   * Permite agrupar los inputs por tab
   * Santiago Blanco
   */
  groupInputs(tabs) {
    var inputs = [];
    return new Promise((resolve, reject) => {
      async.eachSeries(tabs, (tab, cb) => {
        Q.all([
          this.getInputs('left', tab.left.tabs),
          this.getInputs('right', tab.right.tabs)
        ]).then((r) => {
          var newInputs = r[0].concat(r[1])
          inputs = inputs.concat(newInputs)
          cb()
        }).catch((err) => {
          reject(err)
        })
      }, () => {
        // console.log(`finalizando de agrupar ${inputs.length}`)
        resolve(inputs)
      })
    });
  },
  /*
  * Permite guardar CSV con los valores de configuración
  * Santiago Blanco
  */  
  saveCSV(data) {
    return new Promise((resolve, reject) => {
      var dataFormatted = data.replace(/"/g, "");
      fs.writeFile(configCSVPath, dataFormatted, 'utf8', function(err) {
        if (err) {
          //console.log("error", err)
          resolve('No se ha podido guardar');
        } else {
          //console.log('guardado')
          resolve('saved');
        }
      });
    })
  },
  /*
  * Permite convertir de JSON a CSV
  * Santiago Blanco
  */ 
  json2csv(data, header) {
    // console.log("data", data)
    return new Promise((resolve, reject) => {
      var json2csvParser = new json2csv({
        withBOM: false,
        includeEmptyRows: true,
        eol: "\r\n",
        fields: [
          'escalarDemanda',
          'rezago_minutos',
          'rezago_dias',
          'rezago_minuto_dias',
          'pronosticar_RL',
          'pronosticar_GAM',
          'pronosticar_ARIMA',
          'pronosticar_ELMAN',
          'pronosticar_LSTM',
          'k',
          'Ar',
          'I',
          'Ma',
          'capasElman',
          'neuronasCapa1Elman',
          'neuronasCapa2Elman',
          'maxitElman',
          'learnRateElman',
          'capasLSTM',
          'neuronasCapa1LSTM',
          'neuronasCapa2LSTM',
          'activacionCapa1LSTM',
          'activacionCapa2LSTM',
          'batchSizeLSTM',
          'maxitLSTM',
          'learnRateLSTM',
          'nivelConf',
          'nDatosFilt',
          'nDatosEnt',
          'PenFiltrHP',
          'diasPronostico',
          'metodo_comb',
          'nPerAntCorrec',
          'alphaCorrec',
          'kClusters',
          'nDatosClust',
          'Norma',
          'filtrarBarras',
          'Construye_BD',
          'lambdaBD',
          'nDiasBloque',
          'numBloque',
          'nPasosGrafCorr',
          'nDiasHistComb'
        ]
      });
      const csv = json2csvParser.parse(data);
      // console.log("csv", csv)
      resolve(csv)
    })
  },
  /*
  * Permite convertir de CSV a JSON
  * Santiago Blanco
  */ 
  csv2json(csvFilePath) {
    return csvtojson().fromFile(csvFilePath)
  }
};
