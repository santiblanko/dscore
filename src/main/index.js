// dependencias requeridas de electron
import { app, BrowserWindow, ipcMain, globalShortcut } from "electron";
// puente entre los modelos y la solución informatica
import Exec from "./libraries/Exec";
// configuración por defecto de la solución
import defaultConfig from "../../config.json";
// helpers de la solución
import helpers from "./libraries/helper";
// import { create } from 'domain';
// manejo de arrays
import _ from "lodash";
// permite manipular file system
import fs from "fs";
// escuchadores
const watch = require("node-watch");
// manejo de promesas
const Q = require("q");
// manejo de archivos de configuración
const storage = require("electron-json-storage");
// manejo de rutas
const path = require("path");



// archivos que cambian
var watcherResPron = null
var watcherRelog = null
var watcherPid = null
var watcherMPE = null
var watcherResMAPE = null
var watcherResDurac = null

// Define la url
let mainWindow;
const winURL =
  process.env.NODE_ENV === "development"
    ? `http://localhost:9080`
    : `file://${__dirname}/index.html`;

// almacena la información de configuración
var info = {};
// chartData
var chartData = [];

//resMAPE
var resMAPE = []

var MAPE = []

var resDurac = []

// reloj
var clock = null;

//
var mainEvent = null;

console.log("Argumentos de entrada", process.argv);

if (process.env.NODE_ENV !== "development") {
  global.__static = path.join(__dirname, "/static").replace(/\\/g, "\\\\");
}

// Permite obtener la ruta donde guarda la data
const defaultDataPath = storage.getDefaultDataPath();
console.log(
  "La ruta donde se estan cargando los archivos de configuración es: " +
    defaultDataPath
);

// scope de el proceso en R en ejecución
var bash = null;
// pid del proceso en R en ejecución
var pid = null;
// muy importante (Modo debugging)
var DEBUG = true;

var old_console_log = console.log;

/*
 * Permite desabilitar conole.log
 * Santiago Blanco
 */
console.log = function() {
  if (DEBUG) {
    old_console_log.apply(this, arguments);
  }
};

/*
 * Permite iniciar el script en R.
 * Santiago Blanco
 */
function startModelExecution(event) {
  if (_.isNil(pid)) {
    registerWatchers(event);
    mainEvent = event;
    bash = Exec.executeCommand(info);
    pid = bash.pid;

    fs.writeFile(path.resolve("./scriptsxm/pid.txt"), pid, err => {
      if (err) {
        console.log(err);
      } else {
        sendPid(event);
      }
    });
    // al recibir datos emite los datos al frontend
    bash.stdout.on("data", function onGetDataFromTerminal(data) {
      // console.log("cambiando", data)
      fs.writeFile(
        path.resolve("./scriptsxm/stream.txt"),
        data.toString(),
        function(err) {
          if (err) {
            console.log(err);
          }
        }
      );
    });

    bash.stderr.on("data", function onGetErrorDataFromTerminal(data) {
      // console.log("cambiando", data)
      fs.writeFile(
        path.resolve("./scriptsxm/stream.txt"),
        data.toString(),
        function(err) {
          if (err) {
            console.log(err);
          }
        }
      );
    });

    bash.on("exit", function onExit(code) {
      killModelExecution(event);
    });
  }
}

/*
 * Se encarga de finalizar la ejecución del modelo
 * Santiago Blanco
 */
function killModelExecution(event, arg) {
  if (!_.isNil(pid)) {
    bash.kill();
    pid = null;

    watcherResPron.close()
    watcherRelog.close()
    watcherMPE.close()
    watcherResMAPE.close()
    watcherResDurac.close()

    fs.writeFile(path.resolve("./scriptsxm/pid.txt"), pid, err => {
      if (err) {
        watcherPid.close()
        console.log(err);
      }
    });
  }

/*  Exec.kill(pid).then(v => {
    bash.kill();
    pid = null;
    fs.writeFile(path.resolve("./scriptsxm/pid.txt"), pid, err => {
      if (err) {
        console.log(err);
      }
    });
  });*/
}

/*
 * Registro de eventos de la app.
 * Santiago Blanco
 */
function registerEvents() {
  dataLoader();
  // asociados a la app
  app.on("ready", createWindow);
  app.on("window-all-closed", windowAllClosed);
  app.on("activate", windowActivate);
  // asociados al chart
  ipcMain.on("getInfo", getInfo);
  /// asociados a la consola
  ipcMain.on("kill", killModelExecution);
  ipcMain.on("start", startModelExecution);
  ipcMain.on("status", sendPid);

  // asociados a la configuración
  ipcMain.on("saveTraining", saveConfig);
  ipcMain.on("fabricReset", fabricReset);
  ipcMain.on("getConfigFile", getConfigFile);
  ipcMain.on("setConfigFile", setConfigFile);
  ipcMain.on("compressResults", compress);
}


/*
 * Permite comprimir
 * Santiago Blanco
 */
function compress (event) {
  console.log("accediendo")
  helpers.comprime().then(() => {
    console.log("finalizado")
     event.sender.send("readyToDownload"); 
  })
}

/*
 * Se encarga de guardar la configuración incial de la app
 * Santiago Blanco
 */
function setInitialConfig() {
  return new Promise((resolve, reject) => {
    storage.set("config", defaultConfig, function(error) {
      if (error) throw error;
      resolve();
    });
  });
}

/*
 * Permite cambiar el rol del usuario
 * Santiago Blanco
 */
function changeRol(event, arg) {
  info.rol = arg;
  event.sender.send("receiveInfo", info);
}

/*
 * Permite obtener información de configuración
 * Santiago Blanco
 */
function getInfo(event, arg) {
  event.sender.send("receiveInfo", info);
  getChartData(event)
  getRESMAPEData(event)
  getMAPE(event)
  getRESDuracData(event)
  getClock(event)
}

function getMAPE(event, arg) {
  event.sender.send("changeMPE", MAPE);
}

/*
 * Permite obtener la información de RESMAPE
 * Santiago Blanco
 */
function getRESMAPEData(event, arg) {
  event.sender.send("changeResMAPE", resMAPE);
}

/*
 * Permite obtener la información de RESMAPE
 * Santiago Blanco
 */
function getRESDuracData(event, arg) {
  event.sender.send("changeResDurac", resDurac);
}

/*
 * Permite obtener la información de los charts
 * Santiago Blanco
 */
function getChartData(event, arg) {
  event.sender.send("receiveChartData", chartData);
}

/*
 * Permite obtener la información de los charts
 * Santiago Blanco
 */
function getClock(event, arg) {
  event.sender.send("changeClock", clock);
}

/*
 * Permite guardar configuración
 * Santiago Blanco
 */
function saveConfig(event, arg) {
  if (typeof arg === "object") {
    console.log("entrando si es objeto");
    var parameters = arg;
  } else {
    console.log("entrando si es array");
    var parameters = JSON.parse(arg);
  }
  // Esto lo necesitamos hacer para generar el csv
  helpers
    .groupInputs(parameters.training.tabs)
    .then(helpers.formatStructure)
    .then(helpers.json2csv)
    .then(helpers.saveCSV)
    .then(() => {
      console.log("Llegando a guardar en el storage", parameters);
      setConfigInStorage(event, parameters);
      //var exec = require("child_process").exec;
      //exec(process.execPath);
      // app.quit();
    });
}

/*
 * Almacena la configuración en el localstorage
 * Santiago Blanco
 */
function setConfigInStorage(event, arg) {
  return new Promise((resolve, reject) => {
    info = arg;
    storage.set("config", arg, function(error) {
        event.sender.send("receiveInfo", info);
      if (error) throw error;
      resolve();
    });
  });
}

/*
 * Permite restaurar opciones de fabrica
 * Santiago Blanco
 */
function fabricReset(event, arg) {
  info = defaultConfig;
  info.rol = false;
  setInitialConfig()
}

/*
 * Retorna el archivo de configuración
 * Santiago Blanco
 */
function getConfigFile(event, arg) {
  getConfig().then(data => {
    event.sender.send("receiveConfig", JSON.stringify(data));
  });
}

/*
 * Retorna el archivo de configuración
 * Santiago Blanco
 */
function setConfigFile(event, arg) {
  setConfigInStorage(event, JSON.parse(arg))
    .then(() => {
      event.sender.send("receiveInfo", info);
    });
}

/*
 * Se encarga de escuchar los archivos CSV
 * Santiago Blanco
 */
function registerWatchers(event) {
  var status = null;
  var dataResponse = "";
  setInterval(
    param => {
      fs.readFile(path.resolve("./scriptsxm/stream.txt"), (err, data) => {
        if (err) {
          console.log("Error leyendo", err);
        }

        if (data.toString().trim() === '[1] "PRONOSTICO"') {
          console.log("pronostico");
          status = 1;
        }

        if (data.toString().trim() === '[1] "COMBINACION"') {
          console.log("combinación");
          status = 2;
        }

        if (data.toString().trim() === '[1] "DEAS"') {
          console.log("DEAS");
          status = 3;
        }

        if (data.toString().trim() === '[1] "DESAGREGACION"') {
          console.log("Desagregación");
          status = 4;
        }

        if (data.toString().trim() === '[1] "CORRECCION"') {
          console.log("Corrección");
          status = 5;
        }

        if (data.toString().trim() === '[1] "NUEVODATO"') {
          console.log("Nuevo Dato");
          status = 6;
        }

        if (data.toString().trim() === '[1] "IMPUTACION"') {
          console.log("Imputación");
          status = 7;
        }

        if (data.toString().trim() === '[1] "FILTRADO"') {
          console.log("Filtrado");
          status = 8;
        }

        if (data.toString().trim() === '[1] "ACTUALIZACION"') {
          console.log("Actualización");
          status = 9;
        }

        if (data.toString().trim() === '[1] "ENTRENAMIENTO"') {
          console.log("Entrenamiento");
          status = 10;
        }
        if (pid) {
          // envia el paso en el que se encuentra
          param.sender.send("terminalStep", status);
          // envia la salida de la consola
          param.sender.send("terminal-records", data.toString().trim());
        }
      });
    },
    5000,
    event
  );

  // permite escuchar cambios en archivo de resultados
  watcherResPron = watch(path.resolve("./scriptsxm/Res_Pron.csv"), {}, function(evt, name) {
    console.log("archivo cambiado");
    console.log("%s Cambio el pronostico en real time", name);
      getInitialChartData()
        .then(r => {
          chartData = r;

          event.sender.send("receiveChartData", chartData);
        })
  });

  // permite escuchar cambios en el reloj
  watcherRelog = watch(path.resolve("./scriptsxm/reloj.csv"), function(evt, name) {
    helpers.csv2json(path.resolve("./scriptsxm/reloj.csv")).then(v => {
      event.sender.send("changeClock", v);
    });
  });

  // permite escuchar cambios en el pid
  watcherPid = watch(path.resolve("./scriptsxm/pid.txt"), function(evt, name) {
    console.log("el pid ha cambiado");
    event.sender.send("change-status", pid);
  });

  // permite escuchar cambios en el MPE
  watcherMPE = watch(path.resolve("./scriptsxm/MPE.csv"), function(evt, name) {
    helpers.csv2json(path.resolve("./scriptsxm/MPE.csv")).then(v => {
      event.sender.send("changeMPE", v);
    });
  });

  // permite escuchar cambios en el MPE
  watcherResMAPE = watch(path.resolve("./scriptsxm/Res_MAPE.csv"), function(evt, name) {
    helpers.csv2json(path.resolve("./scriptsxm/Res_MAPE.csv")).then(v => {
      event.sender.send("changeResMAPE", v);
    });
  });


  // permite escuchar cambios en el MPE
  watcherResDurac = watch(path.resolve("./scriptsxm/Res_Durac.csv"), function(evt, name) {
    helpers.csv2json(path.resolve("./scriptsxm/Res_Durac.csv")).then(v => {
      event.sender.send("changeResDurac", v);
    });
  });
}
/*
 * Se encarga de obtener la configuración inicial de la app.
 * Santiago Blanco
 */
function getConfig() {
  return new Promise((resolve, reject) => {
    storage.get("config", (error, data) => {
      if (error) throw error;
      if (_.isEmpty(data)) {
        info = defaultConfig;
        setInitialConfig();
      } else {
        info = data;
        // info = defaultConfig
      }
      resolve(info);
    });
  });
}

// obtiene la información
getConfig();

/*
 * Permite obtener información de R
 * Santiago Blanco
 */
function getRInfo() {
  return new Promise((resolve, reject) => {
    Q.all([Exec.getRVersion(info), Exec.getRPath(info)])
      .then(v => {
        info.RVersion = v[0];
        info.RPath = v[1];
        resolve();
      })
      .catch(err => {
        console.log("entrando a error", err);
        console.log(err.message);
        reject();
      });
  });
}

/*
 * Permite enviar el PID de el script en R en ejecución
 * Santiago Blanco
 */
function sendPid(event, arg) {
  event.sender.send("process-status", pid);
}

/*
 * Función que se ejecuta cuando todas las ventanas se han cerrado
 * Santiago Blanco
 */
function windowAllClosed() {
  if (process.platform !== "darwin") {
    app.quit();
  }
}

/*
 * Función que se ejecuta cuando se activa la pantalla
 * Santiago Blanco
 */
function windowActivate() {
  if (mainWindow === null) {
    createWindow();
  }
}

/*
 * Permite crear la ventana de la app
 * Santiago Blanco
 */
function createWindow() {
  mainWindow = new BrowserWindow({
    title: info.title,
    height: 730,
    resizable: true,
    useContentSize: true,
    width: 1300
  });

  mainWindow.setMenu(null);
  mainWindow.loadURL(winURL);
  mainWindow.webContents.openDevTools()
  mainWindow.on("closed", () => {
    mainWindow = null;
  });
}

/*
 * Permite obtener la información inicial
 * Santiago Blanco
 */
function getInitialChartData() {
  return new Promise((resolve, reject) => {
    Q.all([getInitialChartDataResPron(), getInitialChartDataResCorr()])
      .then(responseChart => {
        resolve(responseChart);
      })
      .catch(error => {
        console.log("error al obtener información", error);
        reject(error);
      });
  });
}
/*
 * Permite obtener la información inicial a renderizar.
 * Santiago Blanco
 */
function getInitialChartDataResPron() {
  return new Promise((resolve, reject) => {
    helpers.csv2json(path.resolve("./scriptsxm/Res_Pron.csv")).then(resolve);
  });
}

/*
 * Permite retornar los archivos de configuración del reloj
 * Santiago Blanco
 */
function getInitialClock() {
  return new Promise((resolve, reject) => {
    helpers.csv2json(path.resolve("./scriptsxm/reloj.csv")).then(resolve);
  });
}

/*
 * Permite obtener la información inicial a renderizar.
 * Santiago Blanco
 */
function getInitialChartDataResCorr() {
  return new Promise((resolve, reject) => {
    helpers.csv2json(path.resolve("./scriptsxm/Res_Corr.csv")).then(resolve);
  });
}

/*
 * Permite obtener la información inicial de RESMAPE
 * Santiago Blanco
 */
function getInitialResMape () {
  return new Promise((resolve, reject) => {
    helpers.csv2json(path.resolve("./scriptsxm/Res_MAPE.csv")).then(resolve);
  });
}

/*
 * Permite obtener la información del MAPE
 * Santiago Blanco
 */
function getInitialMape () {
  return new Promise((resolve, reject) => {
    helpers.csv2json(path.resolve("./scriptsxm/MPE.csv")).then(resolve);
  })
}

/*
 * Permite obtener la información inicial de RESMAPE
 * Santiago Blanco
 */
function getInitialResDurac () {
  return new Promise((resolve, reject) => {
    helpers.csv2json(path.resolve("./scriptsxm/Res_Durac.csv")).then(resolve);
  });
}

/*
 * Permite iniciar los charts y el relog
 * Santiago Blanco
 */
function dataLoader() {

  Q.all([
      getInitialChartData(),
      getInitialResMape(),
      getInitialResDurac(),
      getInitialMape()
    ]).then(r => {
      chartData = r[0];
      resMAPE = r[1];
      resDurac = r[2];
      MAPE = r[3]
    })
    .then(getInitialClock)
    .then(r => {
      clock = r;
    })
    .catch(err => {
      console.log("Error al obtener información inicial de la grafica", err);
    });
}

// register app events
registerEvents();
