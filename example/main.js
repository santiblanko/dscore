// helpers de la solución
const helpers = require("../src/main/libraries/helper");

console.log(helpers)

var areas = [{
		"value": "Quindio",
		"label": "Quindío",
		"subestaciones": [{
			"value": "REGIVI",
			"label": "Regivi"
		}, {
			"value": "TEBAID",
			"label": "Tebaida"
		}, {
			"value": "ARMENIA",
			"label": "Armenia"
		}]
	},{
		"value": "Caribe",
		"label": "Caribe",
		"subestaciones": [{
			"value": "URIBIA",
			"label": "Uribia"
		}, {
			"value": "CARTAGENA",
			"label": "Cartagena"
		}]
	}]



var areasFormatted = []
var subestacionesFormatted = []


var structure = [['',]]
areas.forEach((area) => {
	areasFormatted.push({
		name: area.value
	})
	structure[0].push(area.x)
	if (area.subestaciones.length > 0) {
		area.subestaciones.forEach((subestacion) => {
			subestacionesFormatted.push({
				name:subestacion.value,
				parent: area.value
			})
		})
	}
})



subestacionesFormatted.forEach((subestacion) => {
	var register = [subestacion.name]
	areasFormatted.forEach((area) => {
		if (subestacion.parent === area.name) {
			register.push(1)
		} else {
			register.push(0)
		}
	})
	structure.push(register)
})

console.log(structure)




/*

CARIBE	QUINDIO
CARTAGENA	1	0
URIBIA	1	0
REGIVI	0	1
TEBAID	0	1
ARMENIA	0	1*/