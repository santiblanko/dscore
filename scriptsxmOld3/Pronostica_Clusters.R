#####################################################################################################
################################# PRON�STICO USANDO LOS CLUSTERS ####################################
#####################################################################################################

# Datos de entrada: funci�n escalar, SubestNomb, SubestNomb_f, datosNuevoArea (pron�sticos, d�as), variables de 
# escalamiento, Norma, DiasPronoticoBarras, kClusters,

y_prBarra <- array(NA, dim = c((diasPronostico*288),length(SubestNomb)))
colnames(y_prBarra) <- SubestNomb_p
for (i in 1:(diasPronostico*288))
{
  #print(paste("Procesando el dato",i))
  # Extracci�n de datos del d�a en cuesti�n y hora de an�lisis
  datosTemp <- datosNuevosArea[(contDatos+i),]
  
  # Escalamiento de las variables del cluster
  TemperatEsc <- escalar(datosTemp$Temperatura,minTemperatura,maxTemperatura,TRUE)
  RadEsc <- escalar(datosTemp$Radiacion,minRadiacion,maxRadiacion,TRUE)
  VientoEsc <- escalar(datosTemp$Viento,minViento,maxViento,TRUE)
  HumedadEsc <- escalar(datosTemp$Humedad,minHumedad,maxHumedad,TRUE)
  
  # Construcci�n del vector de variables independientes
  x <- c(TemperatEsc,RadEsc,VientoEsc,HumedadEsc)
  
  dia_temp <- which(datosTemp[,Dias] == 1)
  h <- datosTemp$HORA
  
  for (j in 1:length(SubestNomb_f))
  {
    err <- NULL
    for (k in 1:kClusters)
    {
      if (Norma == 1)
      {
        err[k] <- mean(abs(x - MatCentroides[k,2:(length(x)+1),j,(h+1),dia_temp])) # distancia L1
      }
      if (Norma == 2)
      {
        err[k] <- sqrt(mean((x - MatCentroides[k,2:(length(x)+1),j,(h+1),dia_temp])^2)) # distancia L2
      }
    }
    
    pesosPert <- NULL
    y_prBarra[i,j] <- 0
    for (k in 1:kClusters)
    {
      pesosPert[k] <- ( 1/err[k] ) / (sum(1/err)) # Peso a cada cluster seg�n distancia al centroide
      y_prBarra[i,j] <- y_prBarra[i,j] + pesosPert[k] * MatCentroides[k,1,j,(h+1),dia_temp]
    }
    # Se decodifica el valor escalado de la demanda
    y_prBarra[i,j] <- minDemanda + y_prBarra[i,j] * (maxDemanda - minDemanda)
  }
}


#MAPEBarras <- NULL
for (j in 1:length(SubestNomb_p))
{
  if (filtrarBarras)
  {
    print(paste('Filtrando subestacion ',SubestNomb[j],sep=''))
    y_prBarra[,SubestNomb_p[j]] <- FiltrarHPMod(y_prBarra[,SubestNomb_p[j]],PenFiltrHP)
  }
  #plot(datosNuevosArea[,SubestNomb[j]][1:(diasPronostico*288)], type = 'l', lty = 2, ylim =c(0,50),col = 1,main =c('Pron�stico Barra ',SubestNomb[j], ' con',kClusters,'clusters'), ylab = 'Demanda (MW)',xlab ='periodo')
  #lines(y_prBarra[,SubestNomb_p[j]], col = 2)
  #legend('topleft',c('Real','Pron�stico (clusteres)'),lty=c(2,1), col = c(1,2))
  #MAPEBarras[j] <- mean(abs(y_prBarra[,SubestNomb_p[j]]/datosNuevosArea[,SubestNomb[j]][1:(diasPronostico*288)] - 1))
}