source(paste(ruta,'fun_GenerarDatos.R',sep = ''))

data <- datosBDArea[,c("FECHA","HORA","MIN",AreaNomb,"Humedad","Radiacion","Temperatura","Viento",Dias)]   ### SACO DE LA BASE DE DATOS LAS VARIABLES DE INTERES
y <- data[,AreaNomb]

indicadores <- c(1:rezago_minutos,(1:rezago_dias)*288)### SIRVE PARA LA FUNCION generar_datos_ciclo


# Se escalan las variables regresoras y la demanda para el calculo de rezagos (no la variable dependiente)
data[,AreaNomb] <- escalar(data[,AreaNomb],LimitesRegres[1,'Demanda'],LimitesRegres[2,'Demanda'],TRUE)
for (j in 2:ncol(LimitesRegres))
{
  data[,colnames(LimitesRegres)[j]] <- escalar(data[,colnames(LimitesRegres)[j]],LimitesRegres[1,colnames(LimitesRegres)[j]],LimitesRegres[2,colnames(LimitesRegres)[j]],TRUE)
}


#### MODELOS DE CORTE TRANSVERSAL ----

if (Entrena_GAM || Entrena_RL)
{
  ### 1) SACAR LOS DATOS DE ENTRENAMIENTO Y LOS DATOS PARA PRUEBA PARA LOS MODELOS DE CORTE TRANSVERSAL----
  escalarDemanda <- FALSE
  escalarExogenas <- FALSE
  base <- generar_datos(data[,AreaNomb],rezago_minutos,rezago_dias,rezago_minuto_dias,data = data) ### GENERAMOS LA BASE DE DATOS
  base_datos <- base$data ### SACAMOS LOS DATOS DE LA BASE
  
  #datos3 <- data.frame(DEMANDA=base_datos[,AreaNomb],base_datos[,-c(1,3:4)])## LE AGREGAMOS COMO PRIMERA COLUMNA TEBAID, Y ELIMINAMOS DE LA BASE DE DATOS LA FECHA LA HORA Y TEBAID
  datos3 <- data.frame(DEMANDA=base_datos[,AreaNomb],base_datos[,-c(1,4)])## LE AGREGAMOS COMO PRIMERA COLUMNA TEBAID, Y ELIMINAMOS DE LA BASE DE DATOS LA FECHA LA HORA Y TEBAID
  
  datos3 <- as.matrix(datos3) ### CONVERTIMOS NUESTRA BASE DE DATOS EN MATRIX
  
  #min_x <- min(datos3[,1]) ## SACAMOS EL MINIMO DE LA SERIE
  #max_x <- max(datos3[,1]) ## SACAMOS EL MAXIMO DE LA SERIE
  x1 <- datos3[,-1] ### DEFINIMOS LAS VARIABLES REGRESORAS
  nombre_variables_regresoras <- c("HORA",'MIN',"Humedad","Radiacion","Temperatura","Viento",Dias) ### VECTOR CON EL NOMBRE DE LAS VARIABLES REGRESORAS
  
  #x1[,nombre_variables_regresoras] <- apply(x1[,nombre_variables_regresoras], 2, ff) ### NORMALIZAMOS LAS VARIABLES REGRESORAS
  n=nrow(datos3) ## SACAMOS EL NUMERO DE DATOS QUE TENEMOS
  
  train=seq(1,n-(diasPronostico*288),by=1) ## VECTOR QUE CONTIENE LAS OBSERVACIONES DENTRO DEL CONJUNTO DE ENTRENAMIENTO
  
  ### DEFINIMOS EL CONJUNTO DE ENTRENAMIENTO
  xentrena <- x1[train,]
  yentrena <- tail(datosBDArea[,AreaNomb],n)[1:length(train)] # En caso que no se desee escalar
  #yentrena=datos3[train,1] # En caso que si se desee escalar la demanda
  entrenamiento=data.frame(yentrena,xentrena)
  
  ### DEFINIMOS EL CONJUNTO DE PRUEBA
  xtest=x1[-train,]
  ytest<- tail(datosBDArea[,AreaNomb],diasPronostico*288)
  #ytest=datos3[-train,1]
  test <- data.frame(ytest,xtest)
}

### 2) REGRESION LINEAL----
if (Entrena_RL & Entrenar)
{
  print('Inicio estimacion modelo Regresion Lineal')
  modelo_RL <- lm(yentrena~.+I(HORA^2)+I(HORA^3)+I(HORA^4)+I(HORA^5)+I(MIN^2)+I(MIN^3)+I(MIN^4)+I(MIN^5)-1, data = entrenamiento)### AGREGAMOS POLINOMIOS DE LA HORA
  summary(modelo_RL)
  
  f0 <- predict(modelo_RL,data.frame(xtest))
  MAPE2 <- mean(abs(ytest-f0)/abs(ytest))
  #matplot(cbind(ytest, f0),col=c('red','blue'),type='l')
}


### 3) GAM----  
if (Entrena_GAM & Entrenar)
{
  print('Inicio estimacion modelo GAM')
  k=k
  star_date <- Sys.time()
  modelo_GAM=gam(as.formula(paste("yentrena~",paste(c("s(HORA ,k=k+4)+s( Humedad ,k=k)+s( Radiacion ,k=k)+s( Temperatura ,k=k)+s( Viento ,k=k)+LUNES+MARTES+MIERCOLES+JUEVES+VIERNES+SABADO+DOMINGO+LF+DFNL",
                                                      paste("s(",c(base$names$rezago,base$names$dias,base$names$rezago_dias,base$names$indicador_demanda),",k=k)"),base$names$indicador),collapse = "+")))
                 ,family=gaussian,data=entrenamiento)
  (cuanto_demoro <- Sys.time()-star_date)
  summary(modelo_GAM)
  
  f0 <- predict(modelo_GAM,data.frame(xtest))
  MAPE2 <- mean(abs(ytest-f0)/abs(ytest))
  matplot(cbind(ytest, f0),col=c('red','blue'),type='l')
}

#### MODELOS DE SERIES DE TIEMPO----

if (Entrena_ARIMA || Entrena_ELMAN || Entrena_LSTM)
{
  ### 1) SACAR LOS DATOS DE ENTRENAMIENTO Y LOS DATOS PARA PRUEBA PARA MODELOS DE SERIES DE TIEMPO ----
  
  base <- generar_datos_series_tiempo(data[,AreaNomb],rezago_dias,rezago_minuto_dias,data = data)
  base_datos <- base$data### EN ESTE CASO SI SE DESEA SE PUEDE USAR EL CICLO FOR
  
  #datos3 <- data.frame(DEMANDA=base_datos[,4],base_datos[,-c(1,3:4)])## LE AGREGAMOS COMO PRIMERA COLUMNA TEBAID, Y ELIMINAMOS DE LA BASE DE DATOS LA FECHA LA HORA Y TEBAID
  datos3 <- data.frame(DEMANDA=base_datos[,AreaNomb],base_datos[,-c(1,4)])## LE AGREGAMOS COMO PRIMERA COLUMNA TEBAID, Y ELIMINAMOS DE LA BASE DE DATOS LA FECHA LA HORA Y TEBAID
  
  datos3 <- as.matrix(datos3) ### CONVERTIMOS NUESTRA BASE DE DATOS EN MATRIX
  
  #min_x <- min(datos3[,1]) ## SACAMOS EL MINIMO DE LA SERIE
  #max_x <- max(datos3[,1]) ## SACAMOS EL MAXIMO DE LA SERIE
  x1 <- datos3[,-1]
  nombre_variables_regresoras <- c("HORA",'MIN',"Humedad","Radiacion","Temperatura","Viento",Dias)
  
  #x1[,nombre_variables_regresoras] <- apply(x1[,nombre_variables_regresoras], 2, ff)
  n=nrow(datos3) ## SACAMOS EL NUMERO DE DATOS QUE TENEMOS
  
  train=seq(1,n-(diasPronostico*288),by=1) ## VECTOR QUE CONTIENE LAS OBSERVACIONES DENTRO DEL CONJUNTO DE ENTRENAMIENTO
  
  ### DEFINIMOS EL CONJUNTO DE ENTRENAMIENTO
  xentrena=x1[train,]
  yentrena=datos3[train,1]
  entrenamiento=data.frame(yentrena,xentrena)
  
  ### DEFINIMOS EL CONJUNTO DE PRUEBA
  xtest=x1[-train,]
  ytest=datos3[-train,1]
  test <- data.frame(ytest,xtest)
  
  
  ### 2) MODELO ARIMA ----
  if (Entrena_ARIMA & Entrenar)
  {
    print('Inicio estimacion modelo ARIMA')
    nn <- which(colnames(xentrena)=="DOMINGO")
    start_time <- Sys.time()
    modelo_ARIMA <- Arima(yentrena,order = c(Ar,I,Ma),xreg = xentrena[,-nn])
    (duracion <- Sys.time()-start_time)
    
    summary(modelo_ARIMA)
    
    f0 <- as.numeric(forecast(modelo_ARIMA,xreg=xtest[,-nn])$mean)
    MAPE2 <- mean(abs(ytest-f0)/abs(ytest))
    matplot(cbind(ytest, f0),col=c('red','blue'),type='l')
  }
  
  
  ### 3) RED ELMAN----
  if (Entrena_ELMAN & Entrenar)
  {
    print('Inicio estimacion modelo ELMAN')
    modelo_ELMAN <- elman(xentrena,
                          yentrena,
                          size = c(neuronasCapa1Elman,neuronasCapa2Elman),
                          maxit =maxitElman,
                          learnFuncParams = c(learnRateElman),
                          inputsTest=xtest,
                          targetsTest=ytest)
    plotIterativeError(modelo_ELMAN)
    f0 <- predict(modelo_ELMAN,data.frame(xtest))
    MAPE2 <- mean(abs(ytest-f0)/abs(ytest))
    matplot(cbind(ytest, f0),col=c('red','blue'),type='l')
  }

  
  ### 4) RED LSTM ----
  if (Entrena_LSTM & Entrenar)
  {
    print('Inicio estimacion modelo LSTM')
    
    x <- xtest[,c(nombre_variables_regresoras)]
    x2 <- xtest[,base$names$indicador]
    dim(xentrena) <- c(nrow(xentrena),1,ncol(xentrena))### REDIMENSIONAMOS LOS DATOS DE ENTRADA
    dim(xtest) <- c(nrow(xtest),1,ncol(xtest))### REDIMENSIONAMOS LOS DATOS DE ENTRADA
    yentrena=as.matrix(yentrena)
    ytest=as.matrix(ytest)
    
    modelo_LSTM <- keras_model_sequential() 
    modelo_LSTM %>% 
      layer_simple_rnn(units = neuronasCapa1LSTM,activation = activacionCapa1LSTM, return_sequences = TRUE,input_shape = c(dim(xentrena)[-1])) %>% ## layer_simple_rnn
      layer_simple_rnn(units = neuronasCapa2LSTM,activation = activacionCapa2LSTM, return_sequences = TRUE) %>% 
      #layer_lstm(units = 20,activation = "relu", return_sequences = TRUE )%>% 
      layer_simple_rnn(units = 10) %>% # return a single vector dimension 50
      layer_dense(units = 1) %>%  
      compile(
        loss = 'mse',
        optimizer = optimizer_rmsprop(lr = learnRateLSTM),
        metrics = c('mse','mape')
      )
    modelo_LSTM %>% fit(
      xentrena, yentrena, batch_size = batchSizeLSTM, epochs = maxitLSTM,validation_data = list(xtest,ytest)
    )
    f0 <- predict(modelo_LSTM,xtest)
    MAPE2 <- mean(abs(ytest-f0)/abs(ytest))
    matplot(cbind(ytest, f0),col=c('red','blue'),type='l')
  }
}
