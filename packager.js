var electronInstaller = require('electron-winstaller');

/*
* Permite crear una versión
* Santiago Blanco
*/
var packager = electronInstaller.createWindowsInstaller({
    appDirectory: '/Users/blanko/Desktop/dscore/orchestrator-win32-x64',
    outputDirectory: '/Users/blanko/Desktop/dscore/output-win32-x64',
    authors: 'Universidad de Antioquia',
    exe: 'orchestrator.exe'
  });

/*
* Acción que se ejecuta cuando se termina de generar la versión
* Santiago Blanco
*/
packager.then((e) => {
	console.log("Ha finalizado de generarse el instalador!")
	console.log(e.message)
});
