########################## FUNCI�N FILTRAR USANDO OPTIMIZACION DE Hodrick-Prescott ##############################
FiltrarHPMod2 <- function(y,lambda)
{
  D <- array(0,dim=c(length(y)-2,length(y)))
  for (j in 1:(ncol(D)-2))
  {
    D[j,(j:(j+2))] <- c(1,-2,1)
  }
  D[nrow(D)-1,(nrow(D)-1):nrow(D)] <- c(1,-2)
  D[nrow(D),nrow(D)] <- 1
  
  M <- diag(length(y)) + 2 * lambda * t(D)%*%D
  y_filt <- solve(M,y)
  return(y_filt)
}
#################################################################################################################


########################## FUNCI�N FILTRAR GARANTIZANDO EMPALME SUAVE CON DOS DATOS PREVIOS ##############################
FiltrarHPEmpalme <- function(y,x_ant,lambda)
{
  X_ant <- tail(x_ant,2) # Debe ser un vector con los dos �ltimos valores tal que el filtro suavice la se�al siguiente
  D <- array(0,dim=c(length(y),length(y)+2))
  for (j in 1:(ncol(D)-2))
  {
    D[j,(j:(j+2))] <- c(1,-2,1)
  }
  D[nrow(D)-1,(nrow(D)-1):nrow(D)] <- c(1,-2)
  D[nrow(D),nrow(D)] <- 1
  
  D1 <- D[,1:2]
  D <- D[,-c(1,2)]
  M <- diag(length(y)) + 2 * lambda * t(D)%*%D
  y_filt <- solve(M,y - 2 * lambda * t(D) %*% D1 %*% x_ant)
  return(y_filt)
}
#################################################################################################################