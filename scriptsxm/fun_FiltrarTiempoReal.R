FiltTiempoReal <- function(z,zmed,x,k1,k2,lambda)
{
  # x[1] es x_{n-2}, x[2] es x_{n-1}
  xn <- ( k1*z + k2*zmed - 2*lambda * (x[1] - 2 * x[2]) ) / ( k1 + k2 + 2*lambda)
  return(xn)
}