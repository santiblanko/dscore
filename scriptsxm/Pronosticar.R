source(paste(ruta,'fun_GenerarDatos.R',sep = ''))

# Se define matriz que contendra todos los pronosticos definidos en la configuracion
pronosticos <- array(numeric(),dim = c(288*diasPronostico,length(ModNombSeries2)))
colnames(pronosticos) <- ModNombSeries2
contMod <- 0

nuevosMeteor <- array(numeric(), dim = c(perPron,4*length(SerPron)))
colnames(nuevosMeteor) <- c(SerPron_t,SerPron_r,SerPron_h,SerPron_v)

demReal <- array(numeric(), dim = c(perPron,length(SerPron)))
colnames(demReal) <- SerPron

for (jj in 1:length(SerPron))
{
  ################## ASIGNACI�N DE VARIABLES METEOROL�GICAS A CADA �REA #####################
  
  nuevosMeteor[,SerPron_t[jj]] <- 0
  nuevosMeteor[,SerPron_r[jj]] <- 0
  nuevosMeteor[,SerPron_h[jj]] <- 0
  nuevosMeteor[,SerPron_v[jj]] <- 0
  demReal[,SerPron[jj]] <- 0
  
  if (jj < length(SerPron))
  {
    idx <- which(Jerarquia[,SerPron[jj]] == 1) # Subestaciones que pertenecen al area bajo an�lisis
    
    for (i in 1:length(idx))
    {
      nuevosMeteor[,SerPron_t[jj]] <- nuevosMeteor[,SerPron_t[jj]] + datosNuevosSIN[(contDatos+1):(contDatos+perPron),Subest_t[idx[i]]]*demMax[1,idx[i]]/sum(demMax[1,idx])
      nuevosMeteor[,SerPron_r[jj]] <- nuevosMeteor[,SerPron_r[jj]] + datosNuevosSIN[(contDatos+1):(contDatos+perPron),Subest_r[idx[i]]]*demMax[1,idx[i]]/sum(demMax[1,idx])
      nuevosMeteor[,SerPron_h[jj]] <- nuevosMeteor[,SerPron_h[jj]] + datosNuevosSIN[(contDatos+1):(contDatos+perPron),Subest_h[idx[i]]]*demMax[1,idx[i]]/sum(demMax[1,idx])
      nuevosMeteor[,SerPron_v[jj]] <- nuevosMeteor[,SerPron_v[jj]] + datosNuevosSIN[(contDatos+1):(contDatos+perPron),Subest_v[idx[i]]]*demMax[1,idx[i]]/sum(demMax[1,idx])
      demReal[,SerPron[jj]] <- demReal[,SerPron[jj]] + datosNuevosSIN[(contDatos+1):(contDatos+perPron),Subest[idx[i]]]
    }
  } else
  {
    nuevosMeteor[,'SIN_t'] <- rowMeans(nuevosMeteor[,Areas_t])
    nuevosMeteor[,'SIN_r'] <- rowMeans(nuevosMeteor[,Areas_r])
    nuevosMeteor[,'SIN_h'] <- rowMeans(nuevosMeteor[,Areas_h])
    nuevosMeteor[,'SIN_v'] <- rowMeans(nuevosMeteor[,Areas_v])
    demReal[,'SIN'] <- rowSums(datosNuevosSIN[(contDatos+1):(contDatos+perPron),Subest])
  }
  
  ###########################################################################################################
  
  nombMeteor <- c(paste(SerPron[jj],'_h',sep = ''),paste(SerPron[jj],'_r',sep = ''),paste(SerPron[jj],'_t',sep = ''),paste(SerPron[jj],'_v',sep = ''))
  data <- datosBDSIN[,c("FECHA","HORA",SerPron_f[jj],nombMeteor,Dias)]   ### SE EXTRAEN DE LA BASE DE DATOS LAS VARIABLES DE INTERES
  nombData <- colnames(data)
  nombData[3] <- c(SerPron[jj])
  colnames(data) <- nombData
  
  #data <- rbind(data,datosNuevosSIN[(contDatos+1):(contDatos+288*diasPronostico),c("FECHA","HORA",SerPron[jj],nombMeteor,Dias)])
  aux <- cbind(datosNuevosSIN[(contDatos+1):(contDatos+perPron),c("FECHA","HORA",Dias)],nuevosMeteor[,nombMeteor],datosFict[,SerPron[jj]])
  colnames(aux)[ncol(aux)] <- SerPron[jj]
  data <- rbind(data,aux)
  remove(aux)
  nombData[4:7] <- c("Humedad","Radiacion","Temperatura","Viento")
  colnames(data) <- nombData
  
  # Se escalan las variables regresoras y la demanda para el calculo de rezagos (no la variable dependiente)
  data[,SerPron[jj]] <- escalar(data[,SerPron[jj]],LimitesRegres['Minimo','Demanda',jj],LimitesRegres['Maximo','Demanda',jj],TRUE)
  for (j in 2:ncol(LimitesRegres[,,jj]))
  {
    data[,colnames(LimitesRegres[,,jj])[j]] <- escalar(data[,colnames(LimitesRegres[,,jj])[j]],LimitesRegres['Minimo',j,jj],LimitesRegres['Maximo',j,jj],TRUE)
  }
  
  #### MODELOS DE CORTE TRANSVERSAL ----
  
  if (pronosticar_GAM || pronosticar_RL)
  {
    ### 1) SACAR LOS DATOS DE ENTRENAMIENTO Y LOS DATOS PARA PRUEBA PARA LOS MODELOS DE CORTE TRANSVERSAL----
    base <- generar_datos(data[,SerPron[jj]],rezago_minutos,rezago_dias,rezago_minuto_dias,data) ### GENERAMOS LA BASE DE DATOS
    base_datos <- base$data ### SACAMOS LOS DATOS DE LA BASE
    datos3 <- data.frame(DEMANDA=base_datos[,SerPron[jj]],base_datos[,-c(1,3)])## LE AGREGAMOS COMO PRIMERA COLUMNA TEBAID, Y ELIMINAMOS DE LA BASE DE DATOS LA FECHA LA HORA Y TEBAID
    datos3 <- as.matrix(datos3) ### CONVERTIMOS NUESTRA BASE DE DATOS EN MATRIX
    x1 <- datos3[,-1] ### DEFINIMOS LAS VARIABLES REGRESORAS
    
    ### DEFINIMOS EL CONJUNTO DE PRUEBA
    xFutura <- tail(x1,diasPronostico*288) # Debe tener filas = diasPronostico*288 y todas las columnas de la BD
    #yFutura = rep(-1,diasPronostico*288) # Se llenan los valores futuros en -1 para no dejar vac�a esta parte
  }
  
  #if (pronosticar_RL & Pronosticar)
  if(pronosticar_RL)
  {
    # Pronostico con RL
    
    # Se identifica el modelo guardado en la fase de entrenamiento
    idxMod <- which(ModNombSeries2 == paste(SerPron[jj],'_RL',sep = ''))
    modelo_RL <- ListModelos[[idxMod]]
    
    #y <- yentrena # VERSION ORIGINAL
    #x <- xtest[,c(nombre_variables_regresoras)]
    #x2 <- xtest[,base$names$indicador]
    
    y <- datosBDSIN[,SerPron_f[jj]] # VERSION DIEGO
    x <- xFutura[,c(nombre_variables_regresoras)]
    x2 <- xFutura[,base$names$indicador]
    y_pronosticada <- NULL
    
    for (j in 1:(diasPronostico*288))
    {
      # Se imprime mensaje cada diez pronosticos
      if ( j %% 24 == 0)
      {
        print(paste('Pronosticando ',SerPron[jj],' con RL', '. Periodo ',j,' de ',perPron,sep = '')) 
      }
      
      x_1 <- NULL
      x_1 <- generar_datos_ciclo(escalar(y,LimitesRegres['Minimo','Demanda',jj],LimitesRegres['Maximo','Demanda',jj],TRUE),rezago_minutos,rezago_dias,rezago_minuto_dias)
      #x_1 <- generar_datos_ciclo(y,rezago_minutos,rezago_dias,rezago_minuto_dias)
      x_2 <- x_1$des
      x_1 <- x_1$dd
      x_reg <- as.data.frame(t(c(x[j,],x_1,x_2,x2[j,])),byrow=T)
      colnames(x_reg)=colnames(xFutura)
      y_predict <-predict(modelo_RL,x_reg)
      #print(y_predict)
      #print(x_reg)
      #Sys.sleep(1)
      y_pronosticada <- c(y_pronosticada,y_predict)
      y <- c(y,y_predict)
    }
    
    #MAPE_REGRESION <- round(mean(abs(ytest-y_pronosticada)/abs(ytest)),3)*100
    
    #plot(yFutura,ylab = "Demanda",xlab = paste("MAPE=",MAPE_REGRESION,"%"),main = "Pron�stico con REGRESION",type="l",ylim = c(0,80))
    #lines(y_pronosticada,lty=2,col=2)
    #legend(x="topleft",legend = c("Pronosticada","Real"),col=c(2,1),lty=c(2,1),bty = "n")
    pronosticos[,ModNombSeries2[idxMod]] <- y_pronosticada
  }
  
  #if (pronosticar_GAM & Pronosticar)
  if (pronosticar_GAM)
  {
    # Pronostico con GAM
    idxMod <- which(ModNombSeries2 == paste(SerPron[jj],'_GM',sep = ''))
    modelo_GAM <- ListModelos[[idxMod]]
    
    #y <- yentrena
    #x <- xtest[,c("HORA","Humedad","Radiacion","Temperatura","Viento","LUNES","MARTES",
    #"MIERCOLES","JUEVES","VIERNES","SABADO","DOMINGO","LF","DFNL")]
    #x2 <- xtest[,base$names$indicador]
    
    y <- datosBDSIN[,SerPron_f[jj]] # VERSION DIEGO
    x <- xFutura[,c(nombre_variables_regresoras)]
    x2 <- xFutura[,base$names$indicador]
    y_pronosticada <- NULL
    
    for (j in 1:(diasPronostico*288))
    {
      # Se imprime mensaje cada diez pronosticos
      if ( j %% 24 == 0)
      {
        print(paste('Pronosticando ',SerPron[jj],' con GAM', '. Periodo ',j,' de ',perPron,sep = '')) 
      }
      
      x_1 <- NULL
      x_1 <- generar_datos_ciclo(escalar(y,LimitesRegres['Minimo','Demanda',jj],LimitesRegres['Maximo','Demanda',jj],TRUE),rezago_minutos,rezago_dias,rezago_minuto_dias)
      #x_1 <- generar_datos_ciclo(y,rezago_minutos,rezago_dias,rezago_minuto_dias)
      x_2 <- x_1$des
      x_1 <- x_1$dd
      x_reg <- as.data.frame(t(c(x[j,],x_1,x_2,x2[j,])),byrow=T);colnames(x_reg)=colnames(xFutura)
      y_predict <-predict(modelo_GAM,x_reg)
      #print(y_predict)
      #Sys.sleep(2)
      y_pronosticada <- c(y_pronosticada,y_predict)
      y <- c(y,y_predict)
    }
    
    # MAPE_GAM <- round(mean(abs(ytest-y_pronosticada)/abs(ytest)),3)*100
    # plot(ytest,ylab = "Demanda",xlab = paste("MAPE=",MAPE_GAM,"%"),main = "Pron�stico con GAM",type="l",ylim = c(0,80))
    # lines(y_pronosticada,lty=2,col=2)
    # legend(x="topleft",legend = c("Pronosticada","Real"),col=c(2,1),lty=c(2,1),bty = "n")
    pronosticos[,ModNombSeries2[idxMod]] <- y_pronosticada
  }
  
  #### MODELOS DE SERIES DE TIEMPO----
  
  if (pronosticar_ARIMA || pronosticar_ELMAN || pronosticar_LSTM)
  {
    ### 1) SACAR LOS DATOS DE ENTRENAMIENTO Y LOS DATOS PARA PRUEBA PARA MODELOS DE SERIES DE TIEMPO ----
    
    base <- generar_datos_series_tiempo(data[,SerPron[jj]],rezago_dias,rezago_minuto_dias,data = data)
    base_datos <- base$data### EN ESTE CASO SI SE DESEA SE PUEDE USAR EL CICLO FOR
    
    #datos3 <- data.frame(DEMANDA=base_datos[,4],base_datos[,-c(1,3:4)])## LE AGREGAMOS COMO PRIMERA COLUMNA TEBAID, Y ELIMINAMOS DE LA BASE DE DATOS LA FECHA LA HORA Y TEBAID
    datos3 <- data.frame(DEMANDA=base_datos[,SerPron[jj]],base_datos[,-c(1,3)])## LE AGREGAMOS COMO PRIMERA COLUMNA TEBAID, Y ELIMINAMOS DE LA BASE DE DATOS LA FECHA LA HORA Y TEBAID
    datos3 <- as.matrix(datos3) ### CONVERTIMOS NUESTRA BASE DE DATOS EN MATRIX
    x1 <- datos3[,-1]
    
    ### DEFINIMOS EL CONJUNTO DE PRUEBA
    xFutura = tail(x1,diasPronostico*288) # Debe tener filas = diasPronostico*288 y todas las columnas de la BD
    
    #if (pronosticar_ARIMA & Pronosticar)
    if (pronosticar_ARIMA)
    {
      idxMod <- which(ModNombSeries2 == paste(SerPron[jj],'_AX',sep = ''))
      modelo_ARIMA <- ListModelos[[idxMod]]
      
      #y <- escalar(datosBDSIN[,SerPron_f[jj]],LimitesRegres['Minimo','Demanda',jj],LimitesRegres['Maximo','Demanda',jj],TRUE)
      y <- datosBDSIN[,SerPron_f[jj]]
      x <- xFutura[,c(nombre_variables_regresoras)]
      x2 <- xFutura[,base$names$indicador]
      y_pronosticada <- NULL
      
      #y <- yentrena
      #x <- xtest[,c(nombre_variables_regresoras)]
      #x2 <- xtest[,base$names$indicador]
      #y_pronosticada <- NULL
      x_r <- NULL
      for (j in 1:(diasPronostico*288)) {
        # Se imprime mensaje cada diez pronosticos
        if ( j %% 24 == 0)
        {
          print(paste('Pronosticando ',SerPron[jj],' con ARIMAX', '. Periodo ',j,' de ',perPron,sep = '')) 
        }
        x_1 <- NULL
        x_1 <- generar_datos_ciclo_series_tiempo(y,rezago_dias,rezago_minuto_dias)
        x_2 <- x_1$des
        x_1 <- x_1$dd
        x_reg <- as.data.frame(t(c(x[j,],x_1,x_2,x2[j,])),byrow=T);colnames(x_reg)=colnames(xtest)
        x_r <- rbind(x_r,x_reg)
        y_p <-tail(as.numeric(forecast(modelo_ARIMA,xreg=x_r[,-nn])$mean),1)
        y_pronosticada <- c(y_pronosticada,y_p)
        y <- c(y,y_p);print(paste("Procesando el dato",j))
      }
      # Desescalamiento cuando la variable dependientes esta escalada
      #y_pronosticada <- LimitesRegres['Minimo','Demanda',jj] + y_pronosticada*(LimitesRegres['Maximo','Demanda',jj] - LimitesRegres['Minimo','Demanda',jj])
      
      #MAPE_ARIMA <- round(mean(abs(ytest-y_pronosticada)/abs(ytest)),3)*100
      #MAPE_ARIMA
      
      #plot(ytest,ylab = "Demanda",xlab = paste("MAPE=",MAPE_ARIMA,"%"),main = "Pron�stico con ARIMA",type="l")
      #lines(y_pronosticada,lty=2,col=2)
      #legend(x="topleft",legend = c("Pronosticada","Real"),col=c(2,1),lty=c(2,1),bty = "n")
      #Pronostico_ARIMA <- y_pronosticada
      pronosticos[,ModNombSeries2[idxMod]] <- y_pronosticada
    }
    
    #if (pronosticar_ELMAN & Pronosticar) # ALEJO: POR FAVOR HACER LO DE ESTE IF EN ARIMA Y LSTM
    if(pronosticar_ELMAN)
    {
      idxMod <- which(ModNombSeries2 == paste(SerPron[jj],'_EL',sep = ''))
      modelo_ELMAN <- ListModelos[[idxMod]]
      
      #y <- escalar(datosBDSIN[,SerPron_f[jj]],LimitesRegres['Minimo','Demanda',jj],LimitesRegres['Maximo','Demanda',jj],TRUE)
      y <- datosBDSIN[,SerPron_f[jj]]
      x <- xFutura[,c(nombre_variables_regresoras)]
      x2 <- xFutura[,base$names$indicador]
      y_pronosticada <- NULL
      
      for (j in 1:(diasPronostico*288)) {
        # Se imprime mensaje cada diez pronosticos
        if ( j %% 24 == 0)
        {
          print(paste('Pronosticando ',SerPron[jj],' con Redes ELMAN', '. Periodo ',j,' de ',perPron,sep = '')) 
        }
        x_1 <- NULL
        x_1 <- generar_datos_ciclo_series_tiempo(y,rezago_dias,rezago_minuto_dias)
        x_2 <- x_1$des
        x_1 <- x_1$dd
        x_reg <- as.matrix(t(c(x[j,],x_1,x_2,x2[j,])),byrow=T);colnames(x_reg)=colnames(xtest)
        y_predict <-predict(modelo_ELMAN,x_reg)
        y_pronosticada <- c(y_pronosticada,y_predict)
        y <- c(y,y_predict);print(paste("Procesando el dato",j))
      }
      # Desescalamiento cuando la variable dependientes esta escalada
      #y_pronosticada <- LimitesRegres['Minimo','Demanda',jj] + y_pronosticada*(LimitesRegres['Maximo','Demanda',jj] - LimitesRegres['Minimo','Demanda',jj])
      
      #MAPE_ELMAN <- round(mean(abs(ytest-y_pronosticada)/abs(ytest)),3)*100
      #MAPE_ELMAN
      
      #plot(ytest,ylab = "Demanda",xlab = paste("MAPE=",MAPE_ELMAN,"%"),main = "Pron�stico con ELMAN",type="l")
      #lines(y_pronosticada,lty=2,col=2)
      #legend(x="topleft",legend = c("Pronosticada","Real"),col=c(2,1),lty=c(2,1),bty = "n")
      #Pronostico_ELMAN <- y_pronosticada
      pronosticos[,ModNombSeries2[idxMod]] <- y_pronosticada
    }
    
    
    #if (pronosticar_LSTM & Pronosticar)
    if(pronosticar_LSTM)
    {
      idxMod <- which(ModNombSeries2 == paste(SerPron[jj],'_LS',sep = ''))
      modelo_LSTM <- ListModelos[[idxMod]]
      
      #y <- escalar(datosBDSIN[,SerPron_f[jj]],LimitesRegres['Minimo','Demanda',jj],LimitesRegres['Maximo','Demanda',jj],TRUE)
      y <- datosBDSIN[,SerPron_f[jj]]
      x <- xFutura[,c(nombre_variables_regresoras)]
      x2 <- xFutura[,base$names$indicador]
      y_pronosticada <- NULL
      
      for (j in 1:(diasPronostico*288)) {
        # Se imprime mensaje cada diez pronosticos
        if ( j %% 24 == 0)
        {
          print(paste('Pronosticando ',SerPron[jj],' con Redes LSTM', '. Periodo ',j,' de ',perPron,sep = '')) 
        }
        
        x_1 <- NULL
        x_1 <- generar_datos_ciclo_series_tiempo(y,rezago_dias,rezago_minuto_dias)
        x_2 <- x_1$des
        x_1 <- x_1$dd
        x_reg <- as.matrix(t(c(x[j,],x_1,x_2,x2[j,])),byrow=T);colnames(x_reg)=colnames(xtest)
        dim(x_reg) <- c(1,dim(xtest)[-1])
        y_p <- predict(modelo_LSTM,x_reg)
        y_pronosticada <- c(y_pronosticada,y_p)
        y <- c(y,y_p);print(paste("Procesando el dato",j))
      }
      
      # Desescalamiento cuando la variable dependientes esta escalada
      #y_pronosticada <- LimitesRegres['Minimo','Demanda',jj] + y_pronosticada*(LimitesRegres['Maximo','Demanda',jj] - LimitesRegres['Minimo','Demanda',jj])
      
      #MAPE_LSTM <- round(mean(abs(ytest-y_pronosticada)/abs(ytest)),3)*100
      #MAPE_LSTM
      
      #plot(ytest,ylab = "Demanda",xlab = paste("MAPE=",MAPE_LSTM,"%"),main = "Pron�stico con LSTM",type="l")
      #lines(y_pronosticada,lty=2,col=2)
      #legend(x="topleft",legend = c("Pronosticada","Real"),col=c(2,1),lty=c(2,1),bty = "n")
      #Pronostico_LSTM <- y_pronosticada
      pronosticos[,ModNombSeries2[idxMod]] <- y_pronosticada
    }
  }
}