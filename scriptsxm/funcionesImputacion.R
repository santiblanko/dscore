############################ FUNCION PARA DETERMINAR LA MEDIA DE UN DIA TIPO ####################
MediaDia <- function(y) {
  vect <- seq(1,(length(y)-288+1),by=288)
  y_media <- NULL
  for (i in 1:288)
  {
    y_media[i] <- mean((y[vect+i-1]), na.rm = TRUE)
  }
  return(y_media)
}
################################################################################################

############################ FUNCION PARA DETERMINAR LA MEDIA DE UN DIA TIPO ####################
MedianaDia <- function(y) {
  vect <- seq(1,(length(y)-288+1),by=288)
  y_mediana <- NULL
  for (i in 1:288)
  {
    y_mediana[i] <- median((y[vect+i-1]), na.rm = TRUE)
  }
  return(y_mediana)
}
################################################################################################


############################ FUNCION PARA DETERMINAR CUANTIL DE UN DIA TIPO ####################
CuantilDia <- function(y,alpha) {
  vect <- seq(1,(length(y)-288+1),by=288)
  y_cuant <- NULL
  for (i in 1:288)
  {
    y_cuant[i] <- quantile((y[vect+i-1]),probs = alpha, na.rm = TRUE)
  }
  return(y_cuant)
}
################################################################################################


##################### FUNCION PARA DETERMINAR LA DERIVADA DE LA DEMANDA EN kW/s ####################
Derivar <- function(y)
{
  dy <- diff(y,differences = 1)*1000/(5*60)
  dy <- c(dy,0)
  return(dy)
}
################################################################################################

################################### FUNCION VECTORIZAR #################################
# Esta funci�n convierte la matriz con los datos de cada tipo de d�a en un vector equivalente con la BD de tama�o nrow(data)
Vectorizar <- function(DatosMatriz,DiasDatos)
{
  TipoDia <- NULL
  VecDatos <- NULL
  for (i in 1:(nrow(DiasDatos)/288))
  {
    TipoDia[i] <- which(DiasDatos[(288*(i-1) + 1),]==1)
    VecDatos[(288*(i-1) + 1):(288*i)] <- DatosMatriz[,TipoDia[i]]
  }
  return(VecDatos)
}
##########################################################################################

################################### FUNCI�N IMPUTAR #######################################
# La funcion que imputa debe recibir y_orig y nivelConf: imputar(y_orig,y_media,LimInf,LimSup)
Imputar <- function(y,y_media,LimInf,LimSup,porcDatosAnom,criterio)
{
  # Derivada de la se�al original en kW/s
  
  #dy <- diff(y,differences = 1)*1000/(5*60)
  #dy <- c(dy,0)
  dy <- Derivar(y)
  dymax <- quantile(dy, prob=0.9, na.rm = TRUE)
  dymin <- quantile(dy, prob=0.1, na.rm = TRUE)
  
  y_imp <- y
  
  # CRITERIO 1: IMPUTANDO SIEMPRE CON LA MEDIA CUANDO LA SE�AL ESTA POR FUERA DE LOS INTERVALOS DE CONFIANZA O CUANDO LA DERIVADA ES CERO O CUANDO
  # LA DERIVADA ESTA POR FUERA DE SUS INTERVALOS DE CONFIANZA
  if (criterio == 1)
  {
    posImp <- which((y > LimSup | y < LimInf | (dy == 0) | dy > dymax | dy < dymin ))
    for (i in 1:length(posImp))
    {
      y_imp[posImp[i]] <- y_media[posImp[i]]
    }
  }
  
  # CRITERIO 2: IMPUTANDO CON EL LIMITE DEL INTERVALO DE CONFIANZA VIOLADO Y CON LA MEDIA CUANDO LA DERIVADA ES CERO
  if (criterio == 2)
  {
    posImp1 <- which(y > LimSup)
    posImp2 <- which(y < LimInf)
    posImp3 <- which( (y < LimSup & y > LimInf) & dy == 0 )
    #posImp3 <- which( y < LimInf & dy == 0 )
    #posImp3 <- which( dy == 0 )
    
    for (i in 1:length(posImp1))
    {
      y_imp[posImp1[i]] <- LimSup[posImp1[i]]
    }
    for (i in 1:length(posImp2))
    {
      y_imp[posImp2[i]] <- LimInf[posImp2[i]]
    }
    for (i in 1:length(posImp3))
    {
      y_imp[posImp3[i]] <- y_media[posImp3[i]]
    }
    
    # Calculo de la similitud entre el patr�n de un dia y la media del dia
    IAH <- NULL
    for (i in 1:(length(y)/288)) # i recorre cada dia almacenado en y
    {
      #y_pat <- y[(288*(i-1) + 1):(288*i)]/max(y[(288*(i-1) + 1):(288*i)])
      #y_patmedia <- y_media[(288*(i-1) + 1):(288*i)]/max(y_media[(288*(i-1) + 1):(288*i)])
      
      y_pat <- y[(288*(i-1) + 1):(288*i)]
      y_patmedia <- y_media[(288*(i-1) + 1):(288*i)]
      IAH[i] <- sqrt(var(y_pat - y_patmedia))
    }
    DiasIAH_anormal <- which(IAH >= quantile(IAH,probs = 1-porcDatosAnom, na.rm = FALSE))
    posImp4 <- NULL
    for (i in 1:length(DiasIAH_anormal))
    {
      posImp4 <- c(posImp4,(288*(DiasIAH_anormal[i]-1)+1):(288*DiasIAH_anormal[i]))
    }
    
    for (i in 1:length(posImp4))
    {
      y_imp[posImp4[i]] <- y_media[posImp4[i]]
    }
    
  }
  return(y_imp)
}