##############################################################################################################
############################################## C�DIGO MAESTRO ################################################
##############################################################################################################


rm(list=ls())

suppressWarnings(library(CVXR, warn.conflicts=FALSE))

##############################################################################################################
########################################### VARIABLES DE ENTRADA #############################################
##############################################################################################################

# Nivel de confianza para la imputaci�n de datos en tiempo real. Min = 0.6, Max = 0.99
nivelConf <- 0.95

# Periodicidad de filtrado en tiempo real. Min = 1, Max = 288
nDatosFilt <- 72

# Periodicidad de pronostico en tiempo real. Min = 1, Max = 288
nDatosPron <- 12


# Constante de penalidad del filtro Hodrick-Prescott. Min = 1, Max = 100000
PenFiltrHP <- 50


# Nombres de los modelos estimados
ModNomb <- c('RL','GAM','RN')

# N�mero de d�as adelante a pronosticar. Min = 1, Max = 14
diasPronostico <- 14

# Variable que determina si se requiere entrenar modelos o no
Entrenar <- 1

# Variable de entrada selector:
metodo_comb <- 3
# 1) Promedio de pron�sticos
# 2) M�nima suma de residuos al cuadrado
# 3) M�nima varianza

# Dias es variable fija. No cambiar por favor.
Dias <- c('LUNES','MARTES','MIERCOLES','JUEVES','VIERNES','SABADO','DOMINGO','LF','DFNL')



##############################################################################################################
############################ ALISTAMIENTO DE TODAS LAS FUNCIONES A LLAMAR ####################################
##############################################################################################################

source('C:/Users/Diego/Documents/R/Demand Forecasting XM/Codigo Maestro/fun_Filtrar.R')
source('C:/Users/Diego/Documents/R/Demand Forecasting XM/Codigo Maestro/fun_Combinar.R')
source('C:/Users/Diego/Documents/R/Demand Forecasting XM/Codigo Maestro/fun_Corregir.R')
source('C:/Users/Diego/Documents/R/Demand Forecasting XM/Codigo Maestro/fun_MediaBarra.R')
source('C:/Users/Diego/Documents/R/Demand Forecasting XM/Codigo Maestro/fun_MediaArea.R')
source('C:/Users/Diego/Documents/R/Demand Forecasting XM/Codigo Maestro/funcionesImputacion.R')

datosBDArea <- read.csv("C:/Users/Diego/Documents/R/Demand Forecasting XM/BD_Quindio.csv")

# Se debe leer el nombre de las subestaciones de alg�n lado
SubestNomb <- c('REGIVI','TEBAID','ARMENIA')
SubestNomb_f <- c('REGIVI_f','TEBAID_f','ARMENIA_f')
AreaNomb <- 'Quindio'
demandaAreaTemp <- datosBDArea[,AreaNomb]
demandaBarrasTemp <- datosBDArea[,SubestNomb_f]

datosNuevosArea <- read.csv("C:/Users/Diego/Documents/R/Demand Forecasting XM/BD_Quindio_prueba.csv")


# Antes del ciclo se debe tener la matriz de medias, de intervalos de confianza y de errores
MediaAreaTemp <- MediaArea(datosBDArea,AreaNomb,Dias,nivelConf)
MediaBarraTemp <- MediaBarra(datosBDArea,SubestNomb,Dias,nivelConf)

# Se crea espacio para ir guardando nuevos datos del �rea y los correspondientes pron�sticos
MatTemp <- array(numeric(),dim = c(nrow(datosBDArea),length(ModNomb)))


# Esta parte del c�digo es ficticia. Se llenan los pron�sticos hist�ricos para iniciar el proceso
for (mod in 1:length(ModNomb))
{
  MatTemp[,mod] <- datosBDArea[,AreaNomb] * (1+0.02*mod) # Inicializo con un error cualquiera positivo
}
MatTemp <- cbind(MatTemp,rowMeans(MatTemp),rowMeans(MatTemp))

colnames(MatTemp) <- c(ModNomb,'Pron_comb','Pron_corr')
datosBDArea <- cbind(datosBDArea,MatTemp)

##############################################################################################################
      ####################################### ENTRENAMIENTO DE MODELOS #############################################
      ##############################################################################################################    
        #source(agjajaj.Codigocamilo.R)
        # source al c�digo de Camilo para entrenar los modelos seleccionados por el usuario. Guardar los modelos entrenados unicamente
      
      ##############################################################################################################


##############################################################################################################
################################# INICIA EL CICLO DE OPERACI�N EN TIEMPO REAL ################################
##############################################################################################################
contDatos <- 0
contDatosPron <- 0
contDatosFilt <- 0
for (i in seq(1,577,by = 288))
{
  fecha <- datosNuevosArea$FECHA[i]
  dia <- which(datosNuevosArea[i,Dias] == 1)
  diaSig <- which(datosNuevosArea[(i+288),Dias] == 1)
  
	# Se extrae la media para diferentes c�lculos adelante
  mediaAreaDia <- MediaAreaTemp$DemandaMedia[dia] # Media del dia del area


  for (hora in 0:23)
  {
    for (minuto in seq(0,55,by = 5))
    {
      periodo <- 12 * hora + minuto/5 + 1
      
      ##############################################################################################################
      ###################################### PRON�STICO DE DEMANDA #################################################
      ##############################################################################################################
      
      
      # Se debe realizar la retroalimentaci�n de los modelos ajustados durante 14 d�as adelante
      # Se deben tener 3 vectores con 4032 datos cada uno representando los pron�sticos de las 3 subestaciones
	if (contDatosPron == nDatosPron)
	{
      #source(funcionPronosticar.R)
      #y_p <- Pronosticar()
      
      y_pRL <- rep(MediaAreaTemp$DemandaMedia[,dia],diasPronostico) * (1 + rnorm((288*diasPronostico),0,0.03))
      y_pGAM <- rep(MediaAreaTemp$DemandaMedia[,dia],diasPronostico) * (1 + rnorm((288*diasPronostico),-0.05,0.01))
      y_pRN <- rep(MediaAreaTemp$DemandaMedia[,dia],diasPronostico) * (1 + rnorm((288*diasPronostico),0.1,0.03))
      
      y_p <- cbind(y_pRL,y_pGAM,y_pRN)
      colnames(y_p) <- c('RL','GAM','RN')
      
      plot(y_p[,1],type='l', lty = 1, col = 2, ylim=c(0,80), lwd= 2,main = 'Pron�sticos �rea Quindio',ylab='Demanda (MW)', xlab = 'Periodo')
      lines(y_p[,2],col = 3, lty = 1)
      lines(y_p[,3],col = 4, lty = 1)
      legend("bottomright",legend = colnames(y_p),lwd = c(1,1,1),col = c(2,3,4),lty=c(1,1,1))
	contDatosPron <- 0
      }
      
      
      ##############################################################################################################
      ###################################### COMBINACI�N DE PRON�STICOS ############################################
      ##############################################################################################################
      
      # y_p es el pron�stico
      
      # Variable de entrada. N�mero de d�as a usar para el c�lculo de errores hist�ricos en la combinaci�n de pron�sticos. Min = 1, Max = 10
      nDiasHistComb <- 1
      
      y_ant <- subset(datosBDArea,datosBDArea[Dias[dia]]==1)[,ModNomb] # Vector de pron�sticos del tipo de d�a
      y_ref <- subset(datosBDArea,datosBDArea[Dias[dia]]==1)[,AreaNomb] # Vector de pron�sticos del tipo de d�a
      y_c <- Combinar(y_p,tail(y_ant,(288*nDiasHistComb)),tail(y_ref,(288*nDiasHistComb)),metodo_comb)
      
      plot(y_p[,1],type='l', lty = 1, col = 2, ylim=c(0,80), lwd= 2,main = 'Pron�sticos �rea Quindio',ylab='Demanda (MW)', xlab = 'Periodo')
      lines(y_p[,2],col = 3, lty = 1)
      lines(y_p[,3],col = 4, lty = 1)
      lines(y_c, col = 6, lty = 1, lwd = 2)
      legend("bottomright",legend = c(colnames(y_p),'combinaci�n'),lwd = c(1,1,1,2),col = c(2,3,4,6),lty=c(1,1,1,1))
      
      
      ##############################################################################################################
      ######################################### PRON�STICO DE DEAs #################################################
      ##############################################################################################################
      
      if (Dias[dia] == 'DFNL')
      {
        y_ref <- subset(datosBDArea,datosBDArea[Dias[dia]]==1)[,AreaNomb] # pendiente escoger cu�ntos d�as
        yDEA <- PronosticarDEA(y_c,mediaAreaDia,y_ref)
        # Se debe usar la metodolog�a de DEA para obtener los pron�sticos ajustados para el DEA
      }
      ##############################################################################################################
      
      
      
      
      
      ##############################################################################################################
      ################################## DESAGREGACI�N A NIVEL DE SUBESTACIONES ####################################
      ##############################################################################################################
      #for(i in 1:length(SubestNomb))
      #{
        
      #}
      
      
      ##############################################################################################################
      ####################################### CORRECCI�N DE PRON�STICOS ############################################
      ##############################################################################################################
      
      # Variable de entrada. N�mero de periodos a usar para el c�lculo de errores anteriores al actual en la correcci�nde de pron�sticos. Min = 1, Max = 288
      nPerAntCorrec <- 12
      alphaCorrec <- 0.8
      
      pr_ant <- tail(datosBDArea[,'Pron_corr'],nPerAntCorrec) # Vector de pron�sticos inmediatamente anteriores
      dem_ant <- tail(datosBDArea[,AreaNomb],nPerAntCorrec) # Vector de pron�sticos del tipo de d�a
      err <- pr_ant/dem_ant - 1 # Error relativo del pron�stico con respecto al valor de referencia
      
      y_corr <- Corregir(y_c,err,alphaCorrec)
      
      # ALEJO, ORGANIZAR LA GR�FICA MOSTRANDO pr_ant y dem_ant antes de y_c y y_corr usando NA
      plot(y_c[1:288],type='l', lty = 1, col = 2, ylim=c(0,80), lwd= 2,main = 'Pron�stico Corregido �rea Quindio',ylab='Demanda (MW)', xlab = 'Periodo')
      lines(y_corr[1:288],col = 1, lty = 1)
      legend("bottomright",legend = c('Preliminar','Correcci�n'),lwd = c(1,1),col = c(2,1),lty=c(1,1))
      
      # Remove cosas que no son necesarias
      
      
      ##############################################################################################################
      ############################## LECTURA DE NUEVA MEDICI�N DE DEMANDA ##########################################
      ##############################################################################################################
      
      z_nuevaBarras <- datosNuevosArea[i,SubestNomb] # Se suman las demandas de las barras para tener el �rea
      z_nuevaArea <- sum(z_nuevaBarras) # Se suman las demandas de las barras para tener el �rea
      contDatos <- contDatos + 1
	contDatosFilt <- contDatosFilt + 1 # contador de datos para filtrar
	contDatosPron <- contDatosPron + 1 # contador de datos para pronosticar

      ##############################################################################################################
      
      
      
      ##############################################################################################################
      ######################### IMPUTACI�N DE LA NUEVA MEDICI�N EN TIEMPO REAL ########################################
      ##############################################################################################################
      
      # Se imputa �rea seg�n el tipo de d�a y usando intervalos de confianza
      LimInfArea <- MediaAreaTemp$LimiteInferior[periodo,dia]
      LimSupArea <- MediaAreaTemp$LimiteSuperior[periodo,dia]
      z_iArea <- pmax(LimInfArea,pmin(LimSupArea,z_nuevaArea))
      
      ###################################### Se calcula el error del pron�stico ####################################
      
      
	############# IMPUTACION A NIVEL DE BARRAS ##################
      # Se imputan barras seg�n el tipo de d�a y usando intervalos de confianza
      LimInfBarras <- NULL
      LimSupBarras <- NULL
      MediaBarras <- NULL
      for (j in 1:length(SubestNomb))
      {
        LimInfBarras[j] <- MediaBarraTemp$LimiteInferior[periodo,dia,j]
        LimSupBarras[j] <- MediaBarraTemp$LimiteSuperior[periodo,dia,j]
        MediaBarras[j] <- MediaBarraTemp$DemandaMedia[periodo,dia,j] # No es necesaria
      }
      z_iBarras <- pmax(LimInfBarras,pmin(LimSupBarras,z_nuevaBarras))
      
      # Se guardan los nuevos datos en columnas de demanda temporales tanto en barras como en �rea
      demandaAreaTemp <- c(demandaAreaTemp,z_iArea,z_nuevaArea)
      demandaBarrasTemp <- rbind(demandaBarrasTemp,z_iBarras,z_nuevaBarras)
      ##############################################################################################################
      
      
      
      
      
      
      ##############################################################################################################
      ########################################## FILTRADO EN TIEMPO REAL ###########################################
      ##############################################################################################################
      
      # Se calculan los �ndices de las demandas que se van a filtrar. Se filtran nDatosFilt seg�n 
      indicesFilt <- (nrow(demandaBarrasTemp)-nDatosFilt+1):nrow(demandaBarrasTemp)
      
      if (contDatosFilt == nDatosFilt)
      {
	# Tambien se debe validar que la distancia media cuadratica (varianza) con respecto al patr�n de la media este por debajo del umbral t�pico para no imputar la se�al con la media�
	patDia <- tail(demandaAreaTemp,nDatosFilt)/max(tail(demandaAreaTemp,nDatosFilt))

	if (var(patDia - mediaAreaDia/max(mediaAreaDia)) > umbral)
	{
		demandaAreaTemp[indicesFilt,1] <- mediaAreaDia
	}
        z_fBarras <- NULL
        for (j in 1:length(SubestNomb))
        {
          z_fBarras <- cbind(z_fBarras,FiltrarHPMod(demandaBarrasTemp[indicesFilt,j],PenFiltrHP))
          plot(demandaBarrasTemp[indicesFilt,j],type = 'l', xlab = 'Periodo', ylab = 'MW', main = paste('Demanda ',SubestNomb[j],' ',hora,' horas ',': ',minuto,' minutos',sep = ''))
          lines(z_fBarras[,j],col = 3)
        }
        contDatosFilt <- 0

	########### ACTUALIZACION DE BASE DE DATOS, MEDIAS E INTERVALOS DE CONFIANZA ##############
	
	#MediaAreaTemp <- MediaArea(datosBDArea,AreaNomb,Dias,nivelConf)
	#MediaBarraTemp <- MediaBarra(datosBDArea,SubestNomb,Dias,nivelConf)
      }
	# Se actualiza la nueva BD
	subMatdatosBDArea[SubestNombf] <- z_fBarras # Esta matriz tiene todas las columnas de datosBDArea pero solo 288 filas.
	#datosBDArea <- rbind(datosBDArea,subMatdatosBDArea)
      ##############################################################################################################
      
      
	
      
      ##############################################################################################################
      ####################################### ENTRENAMIENTO DE MODELOS #############################################
      ##############################################################################################################
      
      if (Entrenar == 1)
      {
        
        #source(agjajaj.Codigocamilo.R)
        # source al c�digo de Camilo para entrenar los modelos seleccionados por el usuario
      }
      ##############################################################################################################
      
      
      
  
    }
  }
}